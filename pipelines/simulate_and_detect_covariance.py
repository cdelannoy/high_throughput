import argparse, sys, os, yaml
from copy import copy
import numpy as np

from itertools import product
from pathlib import Path
from jinja2 import Template
from snakemake import snakemake
from random import choices

__location__, __base__ = Path(__file__).resolve().parents[0], Path(__file__).resolve().parents[1]
sys.path.append(f'{__location__}/..')
from helper_functions import parse_output_path
from analyse_covariance_simulation import analyse_covariance_simulation

parser = argparse.ArgumentParser(description='Simulate multiple high throughput cases and analyse using '
                                             'covariance detection')
# --- IO ---
parser.add_argument('--out-dir', type=str, required=True)

# --- variable parameters ---
parser.add_argument('--p-covariance', type=float, nargs='+', default=[0.5, 0.6, 0.7, 0.8, 0.9, 0.99])
parser.add_argument('--nb-covariant-pos', type=int, nargs='+', default=[2,3,4,5])
parser.add_argument('--nb-covariant-groups', type=int, nargs='+', default=[1,2,3,4])

# --- non-variable params ---
parser.add_argument('--base-yaml', type=str, default=f'{__location__}/simulate_and_detect_covariance_base.yaml', help='yaml containing first part of simulation params')
parser.add_argument('--alpha', type=float, default=0.95)
parser.add_argument('--nb_seqs', type=int, default=200)
parser.add_argument('--reps', type=int, default=5)

# --- running params ---
parser.add_argument('--cores', type=int, default=4)
parser.add_argument('--dryrun', action='store_true')


args = parser.parse_args()

out_dir = parse_output_path(args.out_dir, clean=True)
params_dir = parse_output_path(out_dir + 'params')
cov_detection_dir = parse_output_path(out_dir + 'cov_detection')

with open(args.base_yaml, 'r') as fh: base_dict = yaml.load(fh, yaml.FullLoader)

var_pos_groups = {vpg: np.arange(base_dict['variable_position_groups'][vpg]['start'],
                                 base_dict['variable_position_groups'][vpg]['end'])
                  for vpg in base_dict['variable_position_groups']}

var_pos_list = np.concatenate(list(var_pos_groups.values()))
nb_var_pos = len(var_pos_list)
successful_combination_list = []
for p_cov, nb_cov_pos, nb_cov_groups, rep in product(args.p_covariance, args.nb_covariant_pos,
                                                     args.nb_covariant_groups, range(args.reps)):
    nb_required_var_pos = nb_cov_groups * nb_cov_pos
    if nb_var_pos < nb_required_var_pos:
        print(f'Cannot run for {nb_cov_groups} covariant groups of {nb_cov_pos} positions: '
              f'{nb_required_var_pos} variable positions required, {nb_var_pos} available\n'
              f'skipping...')
        continue
    remaining_var_pos_list = copy(var_pos_list)
    cur_dict = base_dict.copy()
    cur_dict['covariant_positions'] = {}

    for ncg in range(nb_cov_groups):
        covar_pos_list = [int(x) for x in np.sort(np.random.choice(remaining_var_pos_list, size=nb_cov_pos, replace=False))]
        cur_dict['covariant_positions'][covar_pos_list[0]] = {'covariant': covar_pos_list[1:], 'p_covariance': p_cov}
        remaining_var_pos_list = np.delete(remaining_var_pos_list, np.in1d(remaining_var_pos_list, covar_pos_list))
    with open(f'{params_dir}Pcov{p_cov}_NbCovPos{nb_cov_pos}_NbCovGroups{nb_cov_groups}_rep{rep}.yaml', 'w') as fh:
        yaml.dump(cur_dict, fh, default_flow_style=False)
    successful_combination_list.append((p_cov, nb_cov_pos, nb_cov_groups, rep))

with open(f'{__location__}/simulate_and_detect_covariance.sf', 'r') as fh: template_txt = fh.read()

# render snakemake file
sf_txt = Template(template_txt).render(
    __base__=__base__,
    params_dir=params_dir,
    simdata_dir=parse_output_path(out_dir + 'simdata'),
    cov_detection_dir=cov_detection_dir,
    successful_combination_list=successful_combination_list,
    nb_seqs=args.nb_seqs,
    alpha=args.alpha
)
sf_fn = f'{out_dir}simulate_and_detect_covariance.sf'
with open(sf_fn, 'w') as fh: fh.write(sf_txt)

snakemake(sf_fn, cores=args.cores, dryrun=args.dryrun, keepgoing=True)
analyse_covariance_simulation(cov_detection_dir, params_dir, out_dir + 'analysis')

# should vary:
# - p_covariance
# - number of positions covariant together in 1 group
# - number of covariant groups
