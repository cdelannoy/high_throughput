import argparse, os, sys, re, yaml
import pandas as pd
import numpy as np

from pathlib import Path
import matplotlib.pyplot as plt

import seaborn as sns

sys.path.append(str(Path(__file__).resolve().parents[1]) + '/')
from helper_functions import parse_output_path

def analyse_covariance_simulation(cov_detection_dir, params_dir, out_dir):
    out_dir = parse_output_path(out_dir)
    plot_dir = parse_output_path(out_dir + 'plots')
    fn_dict = {x.name: x for x in Path(cov_detection_dir).glob('*')}
    if params_dir[-1] != '/': params_dir += '/'

    cdf = pd.DataFrame(columns=['pcov', 'nb_cov_pos', 'nb_cov_groups', 'rep',
                               'tp_complete', 'tp_incomplete', 'fp', 'fn']).set_index(['pcov', 'nb_cov_pos', 'nb_cov_groups', 'rep'])

    for fn in fn_dict:

        # Extract parameter run params
        pcov, ncp, ncg, rep = fn.split('_')
        pcov = float(re.search('[0-9.]+', pcov).group(0))
        ncp = int(re.search('[0-9]+', ncp).group(0))
        ncg = int(re.search('[0-9]+', ncg).group(0))
        rep = int(re.search('[0-9]+', rep).group(0))

        with open(str(fn_dict[fn]) + '/hits.yaml', 'r') as fh: pred_dict = yaml.load(fh, yaml.FullLoader)

        # Find and load ground truth parameter file
        params_fn = f'{params_dir}{fn}.yaml'
        if not os.path.isfile(params_fn): continue
        with open(params_fn, 'r') as fh: gt_dict = yaml.load(fh, yaml.FullLoader)['covariant_positions']

        # Compare
        pred_remainder = list(pred_dict)
        tpc, tpi, fn = 0,0,0
        for cv in gt_dict:
            if cv not in pred_dict:
                fn += 1
                continue
            gt_pos, pred_pos = np.array(gt_dict[cv]['covariant']),  np.array(pred_dict[cv]['covariant'])
            gt_complete = np.all(np.in1d(gt_pos, pred_pos))  # all true found
            pred_complete = np.all(np.in1d(pred_pos, gt_pos))  # all pred true
            if gt_complete and pred_complete:
                tpc += 1
            else:
                tpi += 1
            pred_remainder.remove(cv)
        fp = len(pred_remainder)
        cdf.loc[(pcov, ncp, ncg, rep), :] = (tpc, tpi, fp, fn)

    cdf = cdf.astype(float)
    cdf.loc[:, 'tp'] = cdf.tp_complete + cdf.tp_incomplete
    cdf.loc[:, 'precision'] = cdf.tp / (cdf.tp + cdf.fp + [1e-20])
    cdf.loc[:, 'recall'] = cdf.tp / (cdf.tp + cdf.fn)

    # cdf.reset_index('rep', inplace=True)
    lvl_dict = {'pcov': 0, 'nb_cov_pos': 1, 'nb_cov_groups': 2}
    inv_lvl_dict = {lvl_dict[k]: k for k in lvl_dict}
    for param in lvl_dict:
        lock_lvls = [0,1,2]
        lock_lvls.remove(lvl_dict[param])
        for i, sub_df in cdf.groupby(level=lock_lvls):
            sub_df.reset_index(param, inplace=True)
            fig, (pr_ax, re_ax) = plt.subplots(1,2, figsize=(10,5))
            sns.violinplot(x=param, y='precision', color='grey', linewidth=0.1, inner=None, data=sub_df, ax=pr_ax)
            sns.stripplot(x=param, y='precision', size=2, data=sub_df, ax=pr_ax)
            sns.violinplot(x=param, y='recall', color='grey', linewidth=0.1, inner=None, data=sub_df, ax=re_ax)
            sns.stripplot(x=param, y='recall', size=2, data=sub_df, ax=re_ax)
            plt.savefig(f'{plot_dir}{inv_lvl_dict[lock_lvls[0]]}{i[0]}_{inv_lvl_dict[lock_lvls[1]]}{i[1]}.svg')
            plt.close(fig)

    summary_df = pd.DataFrame(columns=['pcov', 'nb_cov_pos', 'nb_cov_groups',
                                       'precision', 'recall']).set_index(['pcov', 'nb_cov_pos', 'nb_cov_groups'])
    cdf_reset_rep = cdf.reset_index('rep')
    for i, sub_df in cdf_reset_rep.groupby(cdf_reset_rep.index):
        summary_df.loc[i, :] = [sub_df.precision.mean(), sub_df.recall.mean()]
    summary_df.to_csv(f'{out_dir}summary_scores.csv')
    cdf.to_csv(f'{out_dir}per_case_scores.csv')


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='analyse output of simulate_and_detect_covariance_simulation pipeline')
    parser.add_argument('--cov-detection-dir', type=str, required=True)
    parser.add_argument('--params-dir', type=str, required=True)
    parser.add_argument('--out-dir', type=str, required=True)
    args = parser.parse_args()
    analyse_covariance_simulation(args.cov_detection_dir, args.params_dir, args.out_dir)
