import argparse, sys, yaml
import pickle
from os.path import dirname
from pathlib import Path
import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt

sys.path.append(str(Path(__file__).resolve().parents[0]))
sys.path.append(str(Path(__file__).resolve().parents[1]))

from helper_functions import str2num_dict, parse_output_path
from ouroboros.corrmut import fit_msa_models
from ouroboros.contacts import compute_couplings

from itertools import combinations
from math import exp, log

def logit(x):
    return log(x / (1-x))

def inv_logit(x):
    return 1 / (1 + exp(-x))

vec_inv_logit = np.vectorize(inv_logit)


def compute_couplings(mods, nb_pos):
    coupling_mat = np.zeros((nb_pos, nb_pos), dtype=float)
    vec_dict = {(i1, i2): [] for i1, i2 in combinations(np.arange(nb_pos), 2)}
    for i1, mod in enumerate(mods):
        coef_mat = mod.coef_.reshape((-1, nb_pos - 1, 4))  # dims:
        for i2 in range(nb_pos):
            if i1 == i2:
                continue
            elif i1 > i2:
                vec_dict[(i2, i1)].extend(list(coef_mat[:, i2, :].flatten()))
            else:  # if i1 < i2, position is shifted by 1, as base itself is not predicted by own model
                vec_dict[(i1, i2)].extend(list(coef_mat[:, i2 - 1, :].flatten()))
            # elif i1 > i2:
            #     coupling_mat[i1, i2] = np.linalg.norm(coef_mat[:, i2, :].flatten())
            # else:
            #     coupling_mat[i1, i2] = np.linalg.norm(coef_mat[:, i2 - 1, :].flatten())
    for i1, i2 in vec_dict:
        c = np.linalg.norm(vec_dict[i1,i2])
        # c = inv_logit(np.linalg.norm(vec_dict[i1,i2]) / 8)  # todo: check if the 8 is correct!
        coupling_mat[i1,i2] = c
        coupling_mat[i2,i1] = c
    return coupling_mat

def normalize_contact_mtx(contact_mtx):
    """
    Apply Average Product Correction to the contact matrix.

    See:
    Dunn, Stanley D., Lindi M. Wahl, and Gregory B. Gloor.
    "Mutual information without the influence of phylogeny or entropy
    dramatically improves residue contact prediction."
    Bioinformatics 24.3 (2007): 333-340
    """
    norm_mtx = np.zeros_like(contact_mtx)

    # Precompute means
    mean_coupling = contact_mtx.mean()
    row_means = contact_mtx.mean(axis=1)
    col_means = contact_mtx.mean(axis=0)

    # Iterate over array to compute the corresponding correction
    for idxs, coupling in np.ndenumerate(contact_mtx):
        apc = (row_means[idxs[0]] * col_means[idxs[1]]) / mean_coupling
        corr_coupling = coupling - apc
        norm_mtx[idxs] = corr_coupling
    return norm_mtx


def get_predictable_seqs(seq_list, mod_list, bmat, nmat):
    """
    For each position, find which sequences exhibit covariance
    """
    out_dict = {}
    seq_array = np.array(seq_list)
    nb_mods = len(mod_list)
    nb_seqs = len(bmat)
    for mi, mod in enumerate(mod_list):
        seqs_hit = seq_array[mod.predict(bmat[:, [i for i in range(nb_mods) if i != mi], :].reshape(nb_seqs, -1)) == nmat[:, mi]]
        seqs_hit = sorted(seqs_hit, key=lambda x: x[mi])
        out_dict[mi] = seqs_hit
    out_dict['all'] = seq_list
    return out_dict


def covariance_detection(seq_list, out_dir, alpha):

    # --- Preprocessing ---

    # to numeric
    nmat = np.vstack([np.vectorize(str2num_dict.get)(list(seq)) for seq in seq_list])

    # remove non-variable positions
    var_bool = np.array([len(np.unique(x)) for x in nmat.T]) > 1
    var_mapping_dict = {int(vi): int(vo[0]) for vi, vo in enumerate(np.argwhere(var_bool))}
    nmat = nmat[:, var_bool]

    # to binary
    nb_seqs, nb_pos = nmat.shape
    bmat = np.stack([nmat == i for i in range(4)], axis=-1)  # all zeros == T, gap is impossible

    # --- logistic model construction (M-step) ---

    # 'coevolutionary' model
    mods, alphas = fit_msa_models(nmat, bmat, 'soft', n_jobs=4, dfmax=5)

    # Extract sequences that showed covariance
    covseq_dict = get_predictable_seqs(seq_list, mods, bmat, nmat)
    with open(f'{out_dir}covariant_seqs.pkl', 'wb') as fh:
        pickle.dump(covseq_dict, fh, protocol=pickle.HIGHEST_PROTOCOL)

    # Extract weights
    w_array = np.dstack([mod.coef_ for mod in mods])
    np.save(f'{out_dir}lm_weights.npy', w_array, allow_pickle=False)

    # Extract couplings
    coupling_mat = compute_couplings(mods, nb_pos)
    # coupling_mat = compute_couplings(mods, nb_pos) / 2
    coupling_mat_norm = normalize_contact_mtx(coupling_mat)
    # p_mat = coupling_mat_norm
    # p_mat = np.vectorize(inv_logit)(coupling_mat)
    # p_mat = np.triu(np.vectorize(inv_logit)(coupling_mat))

    hit_dict = {}
    for hit in np.argwhere(p_mat > alpha):
        p1, p2 = var_mapping_dict[hit[0]], var_mapping_dict[hit[1]]
        if p1 not in hit_dict:
            hit_dict[p1] = {'covariant': [], 'p_covariance': []}
        hit_dict[p1]['covariant'].append(p2)
        hit_dict[p1]['p_covariance'].append(float(p_mat[hit[0], hit[1]]))

    np.savetxt(f'{out_dir}p_mat.csv', p_mat, delimiter=',')
    fig, ax = plt.subplots(figsize=(5,5))
    sns.heatmap(p_mat,
                vmin=0.0, vmax=1.0,
                annot=True, linewidth=.5,
                cbar=False, ax=ax)
    fig.savefig(f'{out_dir}heatmap.svg')
    plt.close(fig)

    with open(f'{out_dir}hits.yaml', 'w') as fh: yaml.dump(hit_dict, fh)
    return p_mat

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Detect covariant positions in grouped sequences.')
    parser.add_argument('--in-txt', type=str, required=True,
                        help='Txt file with sequences, 1 seq per line')
    parser.add_argument('--out-dir', type=str, required=True)
    parser.add_argument('--alpha', type=float, default=0.95)
    args = parser.parse_args()

    out_dir = parse_output_path(args.out_dir)
    with open(args.in_txt, 'r') as fh:
        seq_list = [s.strip() for s in fh.readlines()]
    _ = covariance_detection(seq_list, out_dir, args.alpha)
