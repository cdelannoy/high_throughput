import shutil, os, re
from glob import glob
import matplotlib.pyplot as plt
import seaborn as sns
import numpy as np
import pandas as pd

# order: purine --> pyrimidine: GACT
COMPLEMENT_DICT_NUMERIC = {0:2, 1:3, 2:0, 3:1}
num2str_dict= {0:'G', 1: 'A', 2: 'C', 3: 'T'}
str2num_dict = {num2str_dict[k]: k for k in num2str_dict}
forster_radius = 54.0  # forster radius in Angstrom for Cy3-Cy5, according to Murphy et al. 2004

COMPLEMENT_DICT = {'A': 'T', 'T': 'A', 'C': 'G', 'G': 'C', 'U': 'A'}


def reverse_complement(km, numeric=True):
    if numeric:
        return [COMPLEMENT_DICT_NUMERIC[k] for k in km][::-1]
    return ''.join([COMPLEMENT_DICT[k] for k in km][::-1])


def parse_input_path(in_dir, pattern=None, regex=None):
    if type(in_dir) != list: in_dir = [in_dir]
    out_list = []
    for ind in in_dir:
        if not os.path.exists(ind):
            raise ValueError(f'{ind} does not exist')
        if os.path.isdir(ind):
            ind = os.path.abspath(ind)
            if pattern is not None: out_list.extend(glob(f'{ind}/**/{pattern}', recursive=True))
            else: out_list.extend(glob(f'{ind}/**/*', recursive=True))
        else:
            if pattern is None: out_list.append(ind)
            elif pattern.strip('*') in ind: out_list.append(ind)
    if regex is not None:
        out_list = [fn for fn in out_list if re.search(regex, fn)]
    return out_list


def parse_output_path(location, clean=False):
    """
    Take given path name. Add '/' if path. Check if exists, if not, make dir and subdirs.
    """
    if location[-1] != '/':
        location += '/'
    if clean:
        shutil.rmtree(location, ignore_errors=True)
    if not os.path.isdir(location):
        os.makedirs(location)
    return location


def plot_heatmap(array_3d, pd_idx, no_zeros=True):
    mean_df = pd.DataFrame(array_3d.mean(axis=2), index=pd_idx, columns=pd_idx)
    sd_df = pd.DataFrame(array_3d.std(axis=2), index=pd_idx, columns=pd_idx)
    annot_mat = (mean_df.round(2).astype(str) + '\n±' + sd_df.round(2).astype(str))
    if no_zeros:
        annot_mat[annot_mat == '0.0\n±0.0'] = ''
    # annot_mat.columns = annot_mat.index

    fig, hm_ax = plt.subplots(1, 1, figsize=(12, 12))
    sns.heatmap(data=mean_df, annot=annot_mat, fmt='s', cmap='Blues', ax=hm_ax,
                # cbar_kws={'ticks': np.arange(0, 1.1, 0.25)}
                )
    hm_ax.set_aspect('equal', adjustable='box')
    hm_ax.set_ylabel(None); hm_ax.set_xlabel(None)
    fig.delaxes(fig.axes[1])
    plt.tight_layout()

    # hm_ax.legend(title='frac')
    return fig

def plot_scatter_for_seq(pdf, target_seq_list):
    pred_df = pdf.copy()
    seq_nontarget = 'X' * len(target_seq_list[0])
    pred_df.loc[:, 'mark_seq'] = seq_nontarget
    ts_tups = [(ts.replace('X', '[A-Z]'), ts) for ts in target_seq_list]
    for re_pat, re_str in ts_tups:
        pat = re.compile(re_pat)
        pred_df.loc[pred_df.sequence.apply(lambda x: bool(re.search(pat, x))), 'mark_seq'] = re_str
    label_order = [seq_nontarget] + target_seq_list
    err_list = [pred_df.query(f'mark_seq == "{ms}"').error_abs.mean().round(3) for ms in label_order]
    fig, ax = plt.subplots(figsize=(10, 10))
    sns.kdeplot(x='fret', y='fret_pred', hue='mark_seq', data=pred_df, hue_order=label_order)
    sns.scatterplot(x='fret', y='fret_pred', hue='mark_seq', data=pred_df, hue_order=label_order)
    ax.set_ylim([0.0, 11])
    ax.set_xlim([0.0, 11])
    ax.set_box_aspect(1)
    # ax.plot([0, 1], [0, 1], ax.transAxes, ls='--', c='.3')
    plt.tight_layout()

    plt.legend(title='Seq: mean error', labels=[f'{l}: {e}' for l,e in zip(label_order, err_list)])
    return fig


def get_FRET_distance(efret, limits=None):
    efret = np.clip(efret, 0.01, 0.99)  # avoid nans
    d = (efret ** -1 - 1) ** (1/6) * forster_radius
    if limits is None:
        return d
    return np.clip(d, limits[0], limits[1])


def get_FRET_efficiency(dist):
    """
    return FRET efficiency for a given distance, between cy3-cy5
    """
    return 1.0/((dist / forster_radius) ** 6 + 1)
