# High-throughput analysis toolbox
This repo contains scripts for the analysis of various SPARXS experiments, i.e. high-throughput sequencing data with linked single-molecule experiment data.

## Usage
Using scripts required a miniconda installation. Before first usage, create the miniconda-env. Use the mamba drop-in replacement if you have it (otherwise replace `mamba` by `conda`). From the main repo directory:
```shell
mamba env create -f env.yaml
```
Activate environment before using scripts:
```shell
conda activate high_throughput
```

## Simulation
Simulate equal-length DNA reads with tunable covariance between positions to evaluate covariance detection methods, starting from a single randomly-generatred sequence. Requires a parameter yaml file, dictating read properties and which positions should correlate. Structure should be as follows:

```yaml
std_mutation_prob: 0.90  # [0.0-1.0] float, default probability that nt at a position is altered
sequence_length: 200 # [1,> int, length of sequences generated
fret_default: [0.8, 0.2] # list of floats, mu and std for default FRET 
complementary_groups:  # Define which sequences should show complementarity, because they need to basepair. Leave empty if none
  0:  # int, complementary group ID (arbitrary)
    fwd:  # forward strand start and end indices
      start: 5
      end: 10
    rev:  # reverse strand start and end indices
      start: 75
      end: 80
variable_position_groups: # Define which positions should be mutated. Positions outside this range remain always the same
  0: # int variable position group ID (arbitrary)
    start: 10
    end: 15
covariant_positions:  # Define which positions should covary, meaning that if one is mutated, all the covariant positions are too.
  13:  # int, one position with which other positions covary
    covariant: [52, 53]  # list of int, all other positions with which the first position covaries 
    p_covariance: 0.99  # [0.0,1.0] float, probability with which given positions covary.
fret_patterns:  # Define sequences for which FRET is drawn from a different distribution
  A..T.:  # pattern for which deviating FRET dist is used
    p_draw: 0.95  # probability that this FRET dist is used
    fret: [0.6, 0.1]  # FRET mu and std

```
For example files, see `data_simulation/sim_param_yamls` folder.

## DNA flexibility analysis
Run several analyses sequentually on DNA flexiblity data sets, provided as pickled data sets. From the home directory, run:
```shell
python get_position_importance.py  \
    --in-dir path/to/input/file \
    --out-dir path/to/output \
    --in-format *.csv \  # choose [*.csv] or [*.pkl]. [*.csv] takes simulated format.
    --resolution sequence  # choose [sequence] or [molecule]
```


