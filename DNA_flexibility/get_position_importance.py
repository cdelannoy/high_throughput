import os, sys, re, pickle, argparse, gc
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from sklearn.linear_model import LinearRegression, ElasticNetCV
from sklearn.model_selection import KFold
from sklearn.preprocessing import PolynomialFeatures
from math import ceil

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from data_classes_SHK import exp, SequenceGroup, SequenceGroup_ref, SequenceGroup_ssDNA  # required to load data structures!
import xgboost as xgb

from nn import get_nn_weights

sys.path.append(str(Path(__file__).resolve().parents[1]))
# from data_simulation.generate_data import SimulatedSequenceGroup, ssDNA, Ref, Seq
from helper_functions import parse_input_path, parse_output_path, str2num_dict, plot_heatmap, get_FRET_distance, plot_scatter_for_seq
# from covariance_detection.covariance_detection import covariance_detection
from plot_marked import plot_marked

def get_importance_lm(x_train, x_test, y_train, y_test, seqs_test, bs_idx, pos_idx_list, pred_df_list, imp_df_list, interaction_df_dict):
    lin_mod = LinearRegression().fit(x_train, y_train)
    pred_df = pd.DataFrame({'fret': y_test, 'fret_pred': lin_mod.predict(x_test), 'sequence': seqs_test,
                            'model': 'linear', 'bs_idx': bs_idx})
    wm = lin_mod.coef_.reshape((nb_positions, nb_base_types), order='C')
    importance_list = np.array([np.linalg.norm(v) for v in wm])
    importance_list /= np.max(importance_list)
    importance_df = pd.DataFrame({'position': pos_idx_list,
                                  'importance': importance_list,
                                  'model': 'linear', 'bs_idx': bs_idx})
    pred_df_list.append(pred_df)
    imp_df_list.append(importance_df)


def get_importance_mean(x_train, x_test, y_train, y_test, seqs_test, bs_idx, pos_idx_list, pred_df_list, imp_df_list, interaction_df_dict):
    pred_df = pd.DataFrame({'fret': y_test, 'fret_pred': np.mean(y_train), 'sequence': seqs_test,
                            'model': 'mean_only', 'bs_idx': bs_idx})
    pred_df_list.append(pred_df)


def get_importance_elasticNet(x_train, x_test, y_train, y_test,
                              seqs_test, bs_idx, pos_idx_list,
                              pred_df_list, imp_df_list, interaction_df_dict):
    lin_mod = ElasticNetCV(cv=5).fit(x_train, y_train)
    pred_df = pd.DataFrame({'fret': y_test, 'fret_pred': lin_mod.predict(x_test), 'sequence': seqs_test,
                            'model': 'elasticNet', 'bs_idx': bs_idx})
    wm = lin_mod.coef_.reshape((nb_positions, nb_base_types), order='C')
    importance_list = np.array([np.linalg.norm(v) for v in wm])
    importance_list /= np.max(importance_list)
    importance_df = pd.DataFrame({'position': pos_idx_list,
                                  'importance': importance_list,
                                  'model': 'elasticNet', 'bs_idx': bs_idx})
    pred_df_list.append(pred_df)
    imp_df_list.append(importance_df)


def get_importance_elasticNet_interactions(x_train, x_test, y_train, y_test, seqs_test, bs_idx, pos_idx_list, pred_df_list, imp_df_list, interaction_df_dict):
    pf = PolynomialFeatures(2, interaction_only=True, include_bias=False)
    lin_mod = ElasticNetCV(cv=5).fit(pf.fit_transform(x_train), y_train)
    pred_df = pd.DataFrame({'fret': y_test, 'fret_pred': lin_mod.predict(pf.fit_transform(x_test)), 'sequence': seqs_test,
                            'model': 'elasticNet_interactions', 'bs_idx': bs_idx})
    nb_features = nb_positions * nb_base_types

    # single-term importances
    importance_list_noInteractions = np.linalg.norm(
        lin_mod.coef_[:nb_positions * nb_base_types].reshape((nb_positions, nb_base_types), order='C'),
        axis=1)

    # interaction importances
    mat_ia = np.zeros((nb_features, nb_features), dtype=float)
    mat_ia[np.triu_indices_from(mat_ia, 1)] = lin_mod.coef_[nb_features:]
    mat_ia[np.tril_indices_from(mat_ia, -1)] = mat_ia.T[np.tril_indices_from(mat_ia, -1)]
    mat_ia_noTrace = mat_ia[~np.eye(mat_ia.shape[0], dtype=bool)].reshape(mat_ia.shape[0],
                                                                          -1)  # remove trace --> 1 row per feature
    interaction_importance_list = np.linalg.norm(
        mat_ia_noTrace.reshape((nb_positions, nb_positions * nb_base_types ** 2 - nb_base_types)),
        axis=1)
    importance_list = importance_list_noInteractions + interaction_importance_list
    importance_list /= np.max(importance_list)
    importance_df = pd.DataFrame({'position': pos_idx_list,
                                  'importance': importance_list,
                                  'model': 'elasticNet_interactions', 'bs_idx': bs_idx})

    pred_df_list.append(pred_df)
    imp_df_list.append(importance_df)
    mat_ia_out = np.copy(mat_ia)
    for ti, tv in enumerate(lin_mod.coef_[:nb_positions * nb_base_types]):
        mat_ia_out[ti, ti] = tv
    mat_ia_out = np.abs(mat_ia_out)

    if 'elasticNet_interactions' in interaction_df_dict:
        interaction_df_dict['elasticNet_interactions'].append(mat_ia_out)
    else:
        interaction_df_dict['elasticNet_interactions'] = [mat_ia_out]


def get_importance_xgb(x_train, x_test, y_train, y_test, seqs_test, bs_idx, pos_idx_list, pred_df_list, imp_df_list, interaction_df_dict):
    mod = xgb.XGBRegressor(n_estimators=1000, max_depth=5, eta=0.01, colsample_bytree=1.0, subsample=1.0)
    mod.fit(x_train, y_train)
    pred = mod.predict(x_test)
    pred_df = pd.DataFrame({'fret': y_test, 'fret_pred': pred, 'model': 'xgb', 'bs_idx': bs_idx, 'sequence': seqs_test})
    importance_list = np.linalg.norm(mod.feature_importances_.reshape((nb_positions, nb_base_types), order='C'), axis=1)
    importance_list /= np.max(importance_list)
    importance_df = pd.DataFrame({'position': pos_idx_list,
                                  'importance': importance_list,
                                  'model': 'xgb', 'bs_idx': bs_idx})
    pred_df_list.append(pred_df)
    imp_df_list.append(importance_df)


def get_importance_xgb_interactions(x_train, x_test, y_train, y_test, seqs_test, bs_idx, pos_idx_list, pred_df_list, imp_df_list, interaction_df_dict):
    pf = PolynomialFeatures(2, interaction_only=True, include_bias=False)
    mod = xgb.XGBRegressor(n_estimators=1000, max_depth=5, eta=0.01, colsample_bytree=1.0, subsample=1.0)
    mod.fit(pf.fit_transform(x_train), y_train)
    pred = mod.predict(pf.fit_transform(x_test))
    pred_df = pd.DataFrame({'fret': y_test, 'fret_pred': pred, 'model': 'xgb_interactions', 'bs_idx': bs_idx, 'sequence': seqs_test})
    nb_features = nb_positions * nb_base_types

    # single-term importances
    importance_list_noInteractions = np.linalg.norm(mod.feature_importances_[:nb_positions * nb_base_types].reshape((nb_positions, nb_base_types), order='C'), axis=1)

    # interaction importances
    mat_ia = np.zeros((nb_features, nb_features), dtype=float)
    mat_ia[np.triu_indices_from(mat_ia, 1)] = mod.feature_importances_[nb_features:]
    mat_ia[np.tril_indices_from(mat_ia, -1)] = mat_ia.T[np.tril_indices_from(mat_ia, -1)]
    mat_ia_noTrace = mat_ia[~np.eye(mat_ia.shape[0], dtype=bool)].reshape(mat_ia.shape[0], -1)  # remove trace --> 1 row per feature
    interaction_importance_list = np.linalg.norm(mat_ia_noTrace.reshape((nb_positions, nb_positions * nb_base_types ** 2 - nb_base_types)),
                                                 axis=1)
    importance_list = importance_list_noInteractions + interaction_importance_list
    importance_list /= np.max(importance_list)
    importance_df = pd.DataFrame({'position': pos_idx_list,
                                  'importance': importance_list,
                                  'model': 'xgb_interactions', 'bs_idx': bs_idx})

    pred_df_list.append(pred_df)
    imp_df_list.append(importance_df)
    mat_ia_out = np.copy(mat_ia)
    for ti, tv in enumerate(mod.feature_importances_[:nb_positions * nb_base_types]):
        mat_ia_out[ti, ti] = tv
    if 'xgb_interactions' in interaction_df_dict:
        interaction_df_dict['xgb_interactions'].append(mat_ia_out)
    else:
        interaction_df_dict['xgb_interactions'] = [mat_ia_out]


def parse_pkl(in_fn, resolution, min_nb_molecules, min_fret):

    in_dir = str(Path(in_fn).resolve().parent)
    fret_list, fret_sd_list, seq_list = [], [], []

    # load exp object
    with open(in_fn, 'rb') as output_fobj:
        cur_exp = pickle.load(output_fobj)
        cur_exp.root_path = in_dir
        cur_exp.ssDNA.output_filepath = in_dir
        cur_exp.ref.output_filepath = in_dir

    # extract seqs and efficiencies
    for seq_obj in cur_exp.ssDNA.seq_group:
        nb_molecules = len(seq_obj.e_rep)
        if nb_molecules < min_nb_molecules: continue
        if resolution == 'molecule':
            fret_list.extend(seq_obj.e_rep)
            seq_list.extend([seq_obj.group_name] * len(seq_obj.e_rep))
        elif resolution == 'sequence':
            if seq_obj.e_mean < min_fret: continue
            # fret_list.append(get_FRET_distance(seq_obj.e_mean))
            fret_list.append(seq_obj.e_mean)
            fret_sd_list.append(np.std(seq_obj.e_rep))
            seq_list.append(seq_obj.group_name)
        else:
            raise ValueError('resolution type not recognized')
    fret_array = np.array(fret_list)
    seq_array = np.array(seq_list)

    return fret_array, seq_array


def parse_csv(in_fn, resolution, min_nb_molecules, min_fret):
    exp_df = pd.read_csv(in_fn)
    # fret_array = exp_df.fraction_2_state.to_numpy()
    fret_array = exp_df.fret.to_numpy()
    seq_array = exp_df.sequence.to_numpy()

    return fret_array, seq_array


def get_importance_xgb_interactions_l3(x_train, x_test, y_train, y_test, seqs_test, bs_idx, pos_idx_list, pred_df_list, imp_df_list, interaction_df_dict):
    pf = PolynomialFeatures(3, interaction_only=True, include_bias=False)
    mod = xgb.XGBRegressor(n_estimators=2000, max_depth=5, eta=0.01, colsample_bytree=1.0, subsample=1.0)
    mod.fit(pf.fit_transform(x_train), y_train)
    pred = mod.predict(pf.fit_transform(x_test))
    pred_df = pd.DataFrame({'fret': y_test, 'fret_pred': pred, 'model': 'xgb_interactions3', 'bs_idx': bs_idx, 'sequence': seqs_test})
    importance_list = np.linalg.norm(mod.feature_importances_[:nb_positions * nb_base_types].reshape((nb_positions, nb_base_types), order='C'), axis=1)
    importance_list /= np.max(importance_list)
    importance_df = pd.DataFrame({'position': pos_idx_list,
                                  'importance': importance_list,
                                  'model': 'xgb_interactions3', 'bs_idx': bs_idx})
    pred_df_list.append(pred_df)
    imp_df_list.append(importance_df)


parser = argparse.ArgumentParser(description='Determine which position contributes most to FRET value determination by '
                                             'fitting a linear model and assessing weight magnitude.')
parser.add_argument('--in-dir', type=str, required=True)
parser.add_argument('--in-format', type=str, choices=['*.pkl', '*.csv'], default='*.pkl')
parser.add_argument('--out-dir', type=str, required=True)
parser.add_argument('--nb-folds', type=int, default=10)
parser.add_argument('--scatter-importance-limit', type=float, default=0.1)
parser.add_argument('--resolution', type=str, choices=['molecule', 'sequence'], default='sequence')
parser.add_argument('--min-fret', type=float, default=0.4)
parser.add_argument('--min-nb-molecules', type=int, default=30)
args = parser.parse_args()

out_dir = parse_output_path(args.out_dir)
exp_id_dict = {os.path.splitext(Path(x).name)[0]: x for x in parse_input_path(args.in_dir, args.in_format)}

edf_dict = {}
p_mat_dict = {}

for exp_id in exp_id_dict:
    if args.in_format == '*.pkl':
        fret_array, seq_array = parse_pkl(exp_id_dict[exp_id], args.resolution, args.min_nb_molecules, args.min_fret)
    elif args.in_format == '*.csv':
        fret_array, seq_array = parse_csv(exp_id_dict[exp_id], args.resolution, args.min_nb_molecules, args.min_fret)
    else:
        raise ValueError(f'in_format {args.in_format} not recognized')

    out_subdir = parse_output_path(out_dir + exp_id)

    # Encode sequences numerically
    nmat = np.vstack([np.vectorize(str2num_dict.get)(list(seq)) for seq in seq_array])

    # remove non-variable positions
    var_bool = np.array([len(np.unique(x)) for x in nmat.T]) > 1
    var_mapping_dict = {int(vi): int(vo[0]) for vi, vo in enumerate(np.argwhere(var_bool))}
    var_list = list((var_mapping_dict.values()))
    nmat = nmat[:, var_bool]

    # Encode sequences binary
    bmat = np.stack([nmat == i for i in range(4)], axis=-1).astype(float)
    nb_samples, nb_positions, nb_base_types = bmat.shape
    bmat = bmat.reshape((nb_samples, nb_positions * nb_base_types), order='C')

    # fit models in CV scheme
    kf = KFold(n_splits=args.nb_folds)
    pred_df_list = []
    imp_df_list = []
    interaction_df_dict = {}
    for fi, (train_idx, test_idx) in enumerate(kf.split(bmat)):
        km_args = (bmat[train_idx], bmat[test_idx], fret_array[train_idx], fret_array[test_idx], seq_array[test_idx], fi, var_list,
                   pred_df_list, imp_df_list, interaction_df_dict)
        get_nn_weights(*km_args); gc.collect()
        get_importance_lm(*km_args)
        get_importance_elasticNet(*km_args)
        get_importance_elasticNet_interactions(*km_args)
        # get_importance_xgb(*km_args)
        get_importance_xgb_interactions(*km_args)
        # get_importance_xgb_interactions_l3(*km_args)
        get_importance_mean(*km_args)

    pred_df = pd.concat(pred_df_list).reset_index(drop=True)
    imp_df = pd.concat(imp_df_list).reset_index(drop=True)

    pred_df.loc[:, 'error'] = pred_df.fret - pred_df.fret_pred
    pred_df.loc[:, 'error_abs'] = pred_df.error.abs()
    pred_df.to_csv(f'{out_subdir}pred_df.csv')

    stat_df = pd.DataFrame({'error_mean': pred_df.groupby('model').error_abs.mean().round(3),
                            'error_std': pred_df.groupby('model').error_abs.std().round(3)})
    stat_df.to_csv(f'{out_subdir}stat_df.csv')

    # plot error distribution
    fig,ax = plt.subplots(figsize=(10,5))
    sns.barplot(y='error_abs', x='model', data=pred_df)
    ax.set_ylabel('abs error')
    fig.savefig(f'{out_subdir}error_barplot.svg')
    plt.close(fig)


    # plot residuals
    fig, ax = plt.subplots(figsize=(5, 5))
    sns.kdeplot(x='fret', y='error', hue='model', data=pred_df, ax=ax)
    fig.savefig(f'{out_subdir}residuals.svg')
    plt.close(fig)

    # plot fit
    fig,ax = plt.subplots(figsize=(5,5))
    sns.kdeplot(x='fret', y='fret_pred', hue='model', data=pred_df, ax=ax)
    ax.plot([0,1], [0,1], color='black', linestyle='dashed', transform=ax.transAxes)
    # ax.set_ylim([0.4, 1.0])
    # ax.set_xlim([0.4, 1.0])
    ax.set_box_aspect(1)

    fig.savefig(f'{out_subdir}fit.svg')
    plt.close(fig)

    # plot position importance
    fig,ax = plt.subplots(figsize=(10,5))
    sns.barplot(y='importance', x='position', hue='model', data=imp_df)
    ax.set_ylabel('Importance (normalized)')
    fig.savefig(f'{out_subdir}position_importance.svg')
    plt.close(fig)

    # plot interactions importance
    midx = pd.MultiIndex.from_product((var_list, list(str2num_dict)))
    for mod_id in interaction_df_dict:
        interactions_mat = np.dstack(interaction_df_dict[mod_id])
        fig = plot_heatmap(interactions_mat, midx)
        fig.savefig(f'{out_subdir}interaction_importances_{mod_id}.svg')
        plt.close(fig)

        # plot fret vs pred fret with significant combinations marked
        interactions_mat_mean = np.mean(interactions_mat, axis=2)
        high_interaction_idx = np.argwhere(interactions_mat_mean > 0.05)
        high_interaction_idx = high_interaction_idx[:len(high_interaction_idx) // 2]
        hii_patterns = []
        for hii in high_interaction_idx:
            pat_str = ['X'] * nb_positions
            for x in hii:
                hii_midx = midx[x]
                pat_str[hii_midx[0]-1] = hii_midx[1]
            hii_patterns.append(''.join(pat_str))

        plot_marked(pred_df, out_subdir, hii_patterns, mod_id)
