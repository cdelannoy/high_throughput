import re, argparse, sys
from pathlib import Path

import pandas as pd
import matplotlib.pyplot as plt
import seaborn as sns
sys.path.append(str(Path(__file__).resolve().parents[1]))
from helper_functions import parse_output_path

def generate_regex(str_pat):
    return str_pat.replace('X', '[A-Z]')

def plot_marked(df, out_dir, patterns, mod_id):
    df = df.query(f'model == "{mod_id}"').copy()
    df.loc[:, 'mark_seq'] = 'XXXXX'
    for re_str in patterns:
        pat = re.compile(re_str.replace('X', '[A-Z]'))
        df.loc[df.sequence.apply(lambda x: bool(re.search(pat, x))), 'mark_seq'] = re_str


    fig, ax = plt.subplots(figsize = (10,10), )
    sns.kdeplot(x='fret', y='fret_pred', hue='mark_seq', data=df)
    sns.scatterplot(x='fret', y='fret_pred', hue='mark_seq', data=df)
    ax.set_ylim([0.4, 0.95])
    ax.set_xlim([0.4, 0.95])
    ax.set_box_aspect(1)
    plt.tight_layout()
    fig.savefig(f'{out_dir}fret_vs_pred_marked_{mod_id}.svg')
    plt.close(fig)


    sns.violinplot(y='error', x='mark_seq', data=df)
    plt.savefig(f'{out_dir}violin_error_marked_{mod_id}.svg')
    plt.close()

    sns.violinplot(y='error_abs', x='mark_seq', data=df)
    plt.savefig(f'{out_dir}violin_error_abs_marked_{mod_id}.svg')

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Plot FRET and pred FRET, with option to separate sequence patterns')
    parser.add_argument('--in-df', type=str, required=True)
    parser.add_argument('--out-dir', type=str, required=True)
    parser.add_argument('--patterns', type=str, nargs='+', default=[])
    parser.add_argument('--mod-id', type=str, default='xgb_interactions')
    args = parser.parse_args()

    out_dir = parse_output_path(args.out_dir)
    in_df = pd.read_csv(args.in_df, index_col=0)
    plot_marked(in_df, out_dir, args.patterns, args.mod_id)
