import matplotlib
# matplotlib.use('qt5Agg')
import xarray as xr
import numpy as np
import glob
import os
from scipy import optimize
import matplotlib.pyplot as plt
import pickle
import sys
import random
import pandas as pd
import seaborn as sns
from scipy.stats import pearsonr
import matplotlib.gridspec as gridspec


class SequenceGroup:
    def __init__(self, group_name=''):
        self.output_filepath = ''
        self.group_name = group_name
        self.seq = []
        self.seq_unique = []
        self.mid = []
        self.int_greenex = []
        self.int_redex = []
        self.d_rep = []
        self.a_rep = []
        self.ar_rep = []
        self.e_rep = []
        self.s_rep = []
        self.seq_group = []
        self.gamma = 2
        self.bg = [0, 0, 0]
        self.leakage = 0
        self.G2R_excitation_ratio = 1
        self.s_lims = None
        self.i_lims = None
        return

    def calc_fret(self, fr2ana, int_greenex=None, int_redex=None, fr2ana_r=None):
        if int_greenex is None:
            int_greenex = self.int_greenex
        if int_redex is None:
            int_redex = self.int_redex
        if fr2ana_r == None:
            fr2ana_r = fr2ana
        self.d_rep = []
        self.a_rep = []
        self.ar_rep = []
        self.e_rep = []
        self.s_rep = []
        self.mid = []
        self.seq = []

        n_mol = len(self.mid_org) / 100
        for i, mid in enumerate(self.mid_org):
            if i % 7 == 0:
                sys.stdout.write(f'\rcalculating fret...{i / n_mol:.1f}%')
            use_direct_ex = False
            if use_direct_ex:
                self.g2a_direct = 0.05
                d_mean = (np.mean(int_greenex[mid][0, fr2ana[0]:fr2ana[1]]) - self.bg[0]) * self.gamma
                a_mean = np.mean(int_greenex[mid][1, fr2ana[0]:fr2ana[1]]) - self.bg[1]
                ar_mean = (np.mean(int_redex[mid][1, fr2ana_r[0]:fr2ana_r[1]]) - self.bg[2]) * self.G2R_excitation_ratio
                tmp_d_leak = d_mean * self.leakage
                d_mean = d_mean + tmp_d_leak
                a_mean = a_mean - tmp_d_leak
                a_mean = a_mean - (d_mean + a_mean) * self.g2a_direct
                e_mean = a_mean / (d_mean + a_mean)
                s_mean = (d_mean + a_mean) / (d_mean + a_mean + ar_mean)
            else:
                d_mean = (np.mean(int_greenex[mid][0, fr2ana[0]:fr2ana[1]]) - self.bg[0]) * self.gamma
                a_mean = np.mean(int_greenex[mid][1, fr2ana[0]:fr2ana[1]]) - self.bg[1]
                ar_mean = (np.mean(int_redex[mid][1, fr2ana_r[0]:fr2ana_r[1]]) - self.bg[2]) * self.G2R_excitation_ratio
                tmp_d_leak = d_mean * self.leakage
                d_mean = d_mean + tmp_d_leak
                a_mean = a_mean - tmp_d_leak
                e_mean = a_mean / (d_mean + a_mean)
                s_mean = (d_mean + a_mean) / (d_mean + a_mean + ar_mean)
            self.d_rep.append(d_mean)
            self.a_rep.append(a_mean)
            self.ar_rep.append(ar_mean)
            self.e_rep.append(e_mean)
            self.s_rep.append(s_mean)
            self.mid.append(mid)
            self.seq.append(self.seq_org[mid])

        self.d_rep_org = self.d_rep.copy()
        self.a_rep_org = self.a_rep.copy()
        self.ar_rep_org = self.ar_rep.copy()
        self.e_rep_org = self.e_rep.copy()
        self.s_rep_org = self.s_rep.copy()
        # sys.stdout.write('\r FRET calculated!\n')
        sys.stdout.write('\r')
        return

    def draw_hist_es(self, c_ax_es=None, c_ax_ei=None):
        _e = self.e_rep
        _s = self.s_rep
        _i = [elmt1 + elmt2 for elmt1, elmt2 in zip(self.d_rep, self.a_rep)]

        if c_ax_ei == None or c_ax_es == None:
            fhd = plt.figure(20000, figsize=[9, 4])
            fhd.clf()
            c_ax_es, c_ax_ei = fhd.subplots(1, 2)

        bin_e = np.arange(-0.1, 1.1, 0.01)
        bin_s = np.arange(-0.2, 1.2, 0.01)
        _, _, _, ihd = c_ax_es.hist2d(_e, _s, bins=(bin_e, bin_s), cmin=1, cmap='pink')
        if self.s_lims is not None:
            c_ax_es.plot([-0.1, 1.1], [self.s_lims[0], self.s_lims[0]], 'r-')
            c_ax_es.plot([-0.1, 1.1], [self.s_lims[1], self.s_lims[1]], 'r-')
        # ihd = c_ax_es.gci()
        clims = ihd.get_clim()
        ihd.set_clim(clims[0], (clims[1]-clims[0])*0.5 + clims[0])
        # if clims[0] < clims[1] * 0.5:
        #     ihd.set_clim(clims[0], clims[1] * 0.7)
        c_ax_es.set_xlabel('FRET')
        c_ax_es.set_ylabel('S')

        # plt.subplot(122)
        bin_e = np.arange(-0.1, 1.1, 0.01)
        # tmpi_max = np.max(_i)
        bin_i = np.arange(0, 70000, 500)
        # plt.hist2d(_e, _i, bins=(bin_e, bin_i), cmin=1, cmap='pink')
        _, _, _, ihd = c_ax_ei.hist2d(_e, _i, bins=(bin_e, bin_i), cmin=1, cmap='pink')
        if self.i_lims is not None:
            c_ax_ei.plot([-0.1, 1.1], [self.i_lims[0], self.i_lims[0]], 'r-')
            c_ax_ei.plot([-0.1, 1.1], [self.i_lims[1], self.i_lims[1]], 'r-')
        clims = ihd.get_clim()
        ihd.set_clim(clims[0], (clims[1]-clims[0])*0.5 + clims[0])
        c_ax_ei.set_xlabel('FRET')
        c_ax_ei.set_ylabel('Intensity (a.u.)')
        # plt.draw()
        # plt.pause(0.1)

    def select_molecules(self):
        mol_id_chosen = []
        for i in range(len(self.int_greenex)):
            if self.s_lims[0] < self.s_rep_org[i] < self.s_lims[1] \
                    and self.i_lims[0] < (self.d_rep_org[i] + self.a_rep_org[i]) < self.i_lims[1]:
                mol_id_chosen.append(self.mid_org[i])

        self.d_rep = [self.d_rep_org[mid] for mid in mol_id_chosen]
        self.a_rep = [self.a_rep_org[mid] for mid in mol_id_chosen]
        self.ar_rep = [self.ar_rep_org[mid] for mid in mol_id_chosen]
        self.e_rep = [self.e_rep_org[mid] for mid in mol_id_chosen]
        self.s_rep = [self.s_rep_org[mid] for mid in mol_id_chosen]
        self.mid = [self.mid_org[mid] for mid in mol_id_chosen]
        self.seq = [self.seq_org[mid] for mid in mol_id_chosen]

    def classify_the_sequences(self):
        self.seq_group = []
        for cseq in self.seq_unique:
            self.seq_group.append(SequenceGroup(group_name=cseq))

        for mid, cseq in enumerate(self.seq):
            seq_id = self.seq_unique.index(cseq)
            self.seq_group[seq_id].mid.append(self.mid[mid])
            self.seq_group[seq_id].seq.append(cseq)
            self.seq_group[seq_id].d_rep.append(self.d_rep[mid])
            self.seq_group[seq_id].a_rep.append(self.a_rep[mid])
            self.seq_group[seq_id].ar_rep.append(self.ar_rep[mid])
            self.seq_group[seq_id].e_rep.append(self.e_rep[mid])
            self.seq_group[seq_id].s_rep.append(self.s_rep[mid])

    def apply_min_nmol_requirement(self, min_nmol_required=30):
        self.min_nmol2ana = min_nmol_required
        for c_group in self.seq_group:
            if len(c_group.mid) > self.min_nmol2ana:
                c_group.enough_nmol = True
            else:
                c_group.enough_nmol = False

    def calc_e_hist(self, e, e_bin_edge, npeak=2, debug_mode=True):
        def fn_gauss_multi(x, *p):  # a, xc, w = p
            _n_peaks = int(len(p) / 3)
            y = 0.0
            for pid in range(_n_peaks):
                y += np.exp(-(x - p[pid * 3 + 1]) ** 2 / p[pid * 3 + 2] ** 2 / 2) * p[pid * 3] / p[
                    pid * 3 + 2] / np.sqrt(2 * np.pi)
            return y

        def fit_hist(bin_center, hist_count, n_peaks=2, init_pts=(0.55, 0.3, 0)):
            para_init = []
            para_bounds = []
            bin_center_fc = np.arange(bin_center[0], bin_center[-1], (bin_center[1] - bin_center[0]) / 100)
            for pki in range(n_peaks):
                init_width = 0.05
                init_amp = max(hist_count) * init_width * np.sqrt(2 * np.pi)
                # para_init += [max(hist_count) * (bin_center[1] - bin_center[0]), init_pts[pki], 0.1]
                para_init += [init_amp, init_pts[pki], init_width]
                para_bounds += [np.Inf, 1, 0.2]

            para_bounds = ([0.0, -0.1, 0.0] * n_peaks, para_bounds)
            try:
                coeff, var_matrix = optimize.curve_fit(fn_gauss_multi, bin_center, hist_count, p0=para_init,
                                                       bounds=para_bounds, maxfev=1000)
                if n_peaks == 2:
                    if coeff[1] < coeff[4]:
                        tmp = coeff.copy()
                        coeff[0:3] = tmp[3:6]
                        coeff[3:6] = tmp[0:3]
                fit_curv = [bin_center_fc, fn_gauss_multi(bin_center_fc, *coeff)]
                if n_peaks > 1:
                    for pki in range(n_peaks):
                        fit_curv.append(fn_gauss_multi(bin_center_fc, *coeff[pki * 3:pki * 3 + 3]))
            except:
                fit_curv, coeff = None, [np.nan] * 3 * n_peaks
            return fit_curv, coeff

        e_bin_edge = np.array(e_bin_edge)
        e_bin_center = e_bin_edge[:-1] + (e_bin_edge[1] - e_bin_edge[0]) / 2
        if len(e) > 1:
            hist_cnt, _ = np.histogram(e, bins=e_bin_edge)

            tmp_e = [elmt for elmt in e if elmt > 0.3]
            init_pts = [np.mean(tmp_e)]
            if npeak == 2:
                init_pts.append(0)

            fit_curv, fit_coef = fit_hist(e_bin_center, hist_cnt, n_peaks=npeak, init_pts=init_pts)
        else:
            hist_cnt = np.zeros_like(e_bin_center)
            fit_curv = None
            fit_coef = [np.nan, np.nan, np.nan]

        if debug_mode:
            plt.figure(112)
            plt.clf()
            plt.bar(e_bin_center, hist_cnt, width=e_bin_edge[1] - e_bin_edge[0], align='edge', color=(.5, .5, .6))
            if fit_curv:
                if npeak > 1:
                    for pki in range(npeak):
                        plt.plot(fit_curv[0], fit_curv[2 + pki], c=(.8, 0, 0), linewidth=.5)
                plt.plot(fit_curv[0], fit_curv[1], c=(.5, 0, 0), linewidth=2)
                plt.title(f', {fit_coef[1]:.3f}' + r'$\pm$' + f'{fit_coef[2]:.3f}', fontsize=7)
                plt.xlabel('FRET')
                plt.ylabel('Counts')
                plt.tight_layout()
                plt.draw()
                plt.pause(0.1)
        return e_bin_center, hist_cnt, fit_coef, fit_curv

    def show_e_hist(self, e_bin_center, hist_cnt, fit_coef=None, fit_curv=None, ax=None, keyword='', ticksoff=False):
        if ax is None:
            called_for_an_axis = False
            if fignum is None:
                fignum = 211
            fhd = plt.figure(fignum, figsize=(4, 2.4))
            fhd.clf()
            ax = fhd.add_axes([.3, .25, .65, .65])
        else:
            called_for_an_axis = True
        ax.cla()

        ax.bar(e_bin_center, hist_cnt, width=e_bin_center[1] - e_bin_center[0], align='center', color=(.5, .5, .6))
        if fit_curv is not None:
            npeak = len(fit_curv) - 2
            if npeak == 0:
                npeak = 1
            if npeak > 1:
                for pki in range(npeak):
                    ax.plot(fit_curv[0], fit_curv[2 + pki], c=(.8, 0, 0), linewidth=.5)
            ax.plot(fit_curv[0], fit_curv[1], c=(.5, 0, 0), linewidth=2)

        if not called_for_an_axis:
            if fit_coef is not None:
                ax.set_title(keyword + f' ({fit_coef[0]:.3f}' + r'$\pm$' + f'{fit_coef[1]:.3f})', fontsize=7)
            else:
                ax.set_title(keyword, fontsize=7)
            ax.set_xlabel('FRET')
            ax.set_ylabel('Counts')
            fhd.draw()
            plt.pause(0.1)
            fhd.savefig(
                os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_ehist_all_fig{fhd.number}.eps'),
                format='eps')

    def show_e_hist_of_all_molecules(self, c_ax=None):
        e_bin_edge = np.arange(-0.1, 1, 0.02)
        c_e_bin_center, c_hist_cnt, c_fit_coef, c_fit_curv = self.calc_e_hist(self.e_rep, e_bin_edge,
                                                                              npeak=2, debug_mode=False)
        self.e_hist = {'bin_center': c_e_bin_center,
                                       'hist_cnt': c_hist_cnt,
                                       'fit_coef': c_fit_coef,
                                       'fit_curv': c_fit_curv}
        if len(c_fit_coef) != 0:
            self.e_mean = c_fit_coef[1]
            self.e_std = c_fit_coef[2]
        else:
            self.e_mean = np.nan
            self.e_std = np.nan

        if c_ax == None:
            fhd = plt.figure(figsize=(4, 2.4))
            c_ax = fhd.add_axes([.3, .25, .65, .65])

        self.show_e_hist(e_bin_center=c_e_bin_center, hist_cnt=c_hist_cnt, fit_coef=c_fit_coef,
                         fit_curv=c_fit_curv, ax=c_ax, keyword=c_exp_name)
        c_ax.set_title(c_exp_name + f' ({c_fit_coef[1]:.3f}' + r'$\pm$' + f'{c_fit_coef[2]:.3f})', fontsize=7)
        c_ax.set_xlabel('FRET')
        c_ax.set_ylabel('# molecules')

    def calc_all_idv_e_hist(self):
        print('Calculating FRET for each sequence...')
        _seq_group = self.seq_group

        for group_id in range(len(_seq_group)):
            c_ax = None
            ebin_edge = np.arange(0.3, 1, 0.02)

            e_bin_center, hist_cnt, fit_coef, fit_curv = \
                self.calc_e_hist(e=_seq_group[group_id].e_rep, e_bin_edge=ebin_edge, npeak=1, debug_mode=False)
            _seq_group[group_id].e_hist = {'bin_center': e_bin_center,
                                           'hist_cnt': hist_cnt,
                                           'fit_coef': fit_coef,
                                           'fit_curv': fit_curv}
            if len(fit_coef) != 0:
                _seq_group[group_id].e_mean = fit_coef[1]
                _seq_group[group_id].e_std = fit_coef[2]
            else:
                _seq_group[group_id].e_mean = np.nan
                _seq_group[group_id].e_std = np.nan

        print('FRET for each sequence was calculated!')

    def sort_molecules(self, parameter_names=('n_mol', 'e'), fignum=None):
        for pid, parameter_name in enumerate(parameter_names):
            if parameter_name == 'n_mol':
                sorting_vector = [len(elmt.mid) for elmt in self.seq_group]
            elif parameter_name == 'e':
                # sorting_vector = [elmt.e_mean if hasattr(elmt, 'e_mean') else -1 for elmt in self.seq_group]
                sorting_vector = [elmt.e_mean if elmt.e_mean is not np.nan else -1 for elmt in self.seq_group]
            else:
                sorting_vector = []

            sort_ind = np.argsort(sorting_vector)[-1::-1]
            setattr(self, 'seq_group_sorted_by_' + parameter_name, [self.seq_group[idx] for idx in sort_ind])

            sorted_seq = [elmt.group_name for elmt in getattr(self, 'seq_group_sorted_by_' + parameter_name)]
            sorted_nmol = [len(elmt.mid) for elmt in getattr(self, 'seq_group_sorted_by_' + parameter_name)]
            sorted_e = [elmt.e_mean for elmt in getattr(self, 'seq_group_sorted_by_' + parameter_name)]
            sorted_e_std = [elmt.e_std for elmt in getattr(self, 'seq_group_sorted_by_' + parameter_name)]
            for i, (elmt1, elmt2, elmt3) in enumerate(zip(sorted_seq, sorted_nmol, sorted_e)):
                print(f'{i + 1}: ' + elmt1 + f'({elmt2}) E={elmt3:.3f}')
            print('sorted by ' + parameter_name + '\n\n\n\n\n\n\n\n\n\n\n')

            if fignum is not None:
                # --- Draw N mol distribution
                # fhd = plt.figure(fignum + pid, figsize=(4, 4))
                # plt.clf()
                # plt.plot(sorted_nmol, '.', color=(.5, .5, .6))
                # plt.yscale("log")
                # plt.xlim(-10, 1034)
                # # plt.ylim(1, 2000)
                # plt.xlabel('Sequence index')
                # plt.ylabel('# molecules')
                # plt.grid(True)
                # plt.pause(0.5)
                # plt.tight_layout()
                # plt.pause(0.5)
                # fhd.savefig(os.path.join(self.output_filepath,
                #                          'ana_' + self.group_name + '_nmol_sorted_by_' + parameter_name + f'_fig{fhd.number}.eps'),
                #             format='eps')

                # draw_no_mol_and_FRET_per_seq
                # fhd = plt.figure(fignum + pid, figsize=(8, 6))
                fhd = plt.figure(fignum + pid, figsize=(8, 9))
                fhd.clf()
                plt.subplot(3, 1, 1)
                x = np.arange(len(sorted_seq)) + 1
                plt.plot(x, sorted_nmol, linestyle='none', marker='o', markersize=3,
                         markerfacecolor=(.4, .4, .4), markeredgecolor=(.4, .4, .4))
                plt.yscale("log")
                plt.xlim(0, 1025)
                plt.ylim(1, 1000)
                plt.xlabel('sequence ID')
                plt.ylabel('# molecules')
                # plt.yticks(np.arange(0, 1501, 200))
                plt.grid(True)

                # --- draw FRET per seq
                plt.subplot(3, 1, 2)
                plt.errorbar(x, sorted_e, yerr=sorted_e_std,
                             ecolor=(.8, .8, .8), linestyle='none', marker='o', markersize=1,
                             markerfacecolor=(.4, .4, .4), markeredgecolor=(.4, .4, .4), label='std')
                plt.yticks(np.arange(0.4, 1.01, 0.1))
                plt.xlim(0, 1025)
                plt.ylim(0.4, 1.0)
                # plt.legend(loc='lower right')
                plt.legend(loc='best')
                plt.xlabel('sequence ID')
                plt.ylabel('FRET')
                plt.grid(True)
                plt.pause(0.1)
                # plt.tight_layout()

                # --- extra for sem
                # fhd = plt.figure(fignum + pid + 10000)
                plt.subplot(3, 1, 3)
                sorted_sem = [c_estd / np.sqrt(c_nmol) for c_nmol, c_estd in zip(sorted_nmol, sorted_e_std)]
                plt.plot(x, sorted_sem, linestyle='none', marker='o', markersize=1,
                         markerfacecolor=(.4, .4, .4), markeredgecolor=(.4, .4, .4), label='std')
                plt.yscale('log')
                plt.xlim(0, 1025)
                plt.ylim(0.001, 1)
                # plt.legend(loc='lower right')
                plt.xlabel('sequence ID')
                plt.ylabel('FRET error (sem)')
                plt.grid(True)
                plt.pause(0.1)
                plt.tight_layout()
                fhd.savefig(os.path.join(self.output_filepath,
                                         'ana_' + self.group_name + '_sequences_sorted_by_' + parameter_name + f'_fig{fhd.number}.eps'),
                            format='eps')
                fhd.savefig(os.path.join(self.output_filepath,
                                         'ana_' + self.group_name + '_sequences_sorted_by_' + parameter_name + f'_fig{fhd.number}.png'),
                            format='png')

    def analyze_FRET_error_vs_nmol(self, fignum=None, ax=None):
        def run_bootstrap(e_train, n_mol2anas, n_boot=1000, method='gauss_fit'):
            ebin = np.arange(0.4, 1, 0.02)
            boot_result_std_of_mean = []
            boot_result_mean_of_std = []
            n_set = len(n_mol2anas) / 100
            for ii, n_mol2ana in enumerate(n_mol2anas):
                sys.stdout.write(f'\rbootstrapping...{ii / n_set:.0f}% (n_mol = {n_mol2ana})')
                e_mean_collection = []
                e_std_collection = []

                if method == 'gauss_fit':
                    n_success = 0
                    while n_success < n_boot:
                        e_chosen = random.sample(e_train, k=n_mol2ana)
                        _, _, fit_coef, _ = self.calc_e_hist(e=e_chosen,
                                                        e_bin_edge=ebin,
                                                        npeak=1, debug_mode=False)
                        if not np.isnan(fit_coef[1]):
                            e_mean_collection.append(fit_coef[1])
                            e_std_collection.append(fit_coef[2])
                            n_success += 1
                elif 'simple_mean':
                    for _ in range(n_boot):
                        e_chosen = random.sample(e_train, k=n_mol2ana)
                        # e_chosen = np.random.normal(0.5, 0.07, n_mol2ana)
                        e_mean_collection.append(np.mean(e_chosen))
                        e_std_collection.append(np.std(e_chosen))

                boot_result_std_of_mean.append(np.std(e_mean_collection))
                boot_result_mean_of_std.append(np.mean(e_std_collection))
            sys.stdout.write(f'\rboot strapping done!')
            return boot_result_std_of_mean, boot_result_mean_of_std

        if ax == None:
            is_stand_along_fig = True
            if fignum == None:
                fignum = 5111
            fhd = plt.figure(fignum)
            fhd.clf()
            fhd.set_size_inches(3.2, 6)
            ax = fhd.subplots(2, 1)
        else:
            is_stand_along_fig = False
            ax[0].cla()
            ax[1].cla()

        n_mol2anas1 = [int(np.sqrt(2) ** elmt) for elmt in range(6, 20)]
        n_mol2anas2 = [int(np.sqrt(2) ** elmt) for elmt in range(6, 13)]
        std_of_mean_all, mean_of_std_all = run_bootstrap(self.e_rep, n_mol2anas1, n_boot=100, method='gauss_fit')
        for group_id1, c_group in enumerate(self.seq_group):
            if c_group.group_name == 'TATTTATT':
                break
        std_of_mean_ind1, mean_of_std_ind1 = run_bootstrap(self.seq_group[group_id1].e_rep, n_mol2anas2,
                                                         n_boot=1000, method='gauss_fit')
        for group_id2, c_group in enumerate(self.seq_group):
            if c_group.group_name == 'TAAAAATT':
                break
        std_of_mean_ind2, mean_of_std_ind2 = run_bootstrap(self.seq_group[group_id2].e_rep, n_mol2anas2,
                                                         n_boot=1000, method='gauss_fit')
        # fhd = plt.figure(fignum)
        # fhd.set_size_inches(3.2, 6)
        # plt.clf()
        # plt.subplot(211)
        ax[0].plot(n_mol2anas1, std_of_mean_all, 'o-', color=(.6, .6, .6), label='all seq')
        ax[0].plot(n_mol2anas2, std_of_mean_ind1, 'o-', color=(.6, .3, .3), label=self.seq_group[group_id1].group_name)
        ax[0].plot(n_mol2anas2, std_of_mean_ind2, 'o-', color=(.3, .3, .6),
                 label=self.seq_group[group_id2].group_name)
        ax[0].set_xscale('log')
        ax[0].set_yscale('log')
        ax[0].set_xlim(5, 1E3)
        ax[0].set_ylim(1E-3, 1E-0)
        ax[0].set_xlabel('# molecules')
        ax[0].set_ylabel('FRET Error (SEM)')
        ax[0].grid(True, which='major', linestyle='-', color=(.4, .3, .3))
        ax[0].grid(True, which='minor', dashes=(5, 2), color=(.8, .8, .9))

        # ax[1].subplot(212)
        ax[1].plot(n_mol2anas1, mean_of_std_all, 'o-', color=(.6, .6, .6), label='all molecules')
        ax[1].plot(n_mol2anas2, mean_of_std_ind1, 'o-', color=(.6, .3, .3),
                 label=self.seq_group[group_id1].group_name)
        ax[1].plot(n_mol2anas2, mean_of_std_ind2, 'o-', color=(.3, .3, .6),
                 label=self.seq_group[group_id2].group_name)
        ax[1].set_xscale('log')
        ax[1].set_yscale('log')
        ax[1].set_xlim(5, 1E3)
        ax[1].set_ylim(1E-2, 1E-0)
        ax[1].legend()
        ax[1].set_xlabel('# molecules')
        ax[1].set_ylabel('FRET Error (std)')
        ax[1].grid(True, which='major', linestyle='-', color=(.4, .3, .3))
        ax[1].grid(True, which='minor', dashes=(5, 2), color=(.8, .8, .9))

        if is_stand_along_fig:
            plt.tight_layout()
            plt.pause(0.1)
            fhd.savefig(
                os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_error_vs_nmol_fig{fhd.number}.eps'),
                format='eps')

    def analyze_error_vs_nfr(self, fignum=None, ax=None):
        def get_e_t(mid2ana, n_fr2integrate, n_boot = 1000, n_mol_per_boot = 100):
            e_bin_edge = np.arange(0.3, 1, 0.02)
            rep_e_t_all = []

            for j, n_fr in enumerate(n_fr2integrate):
                sys.stdout.write(f'\r analyzing fr#{j}/{len(n_fr2integrate)}')
                tmp_e_means = []
                n_success = 0
                while n_success < n_boot:
                    mid_inuse = random.sample(mid2ana, k=n_mol_per_boot)
                    tmp_int_train = np.array([self.int_greenex[elmt] for elmt in mid_inuse])
                    tmp_int_train_d = np.mean(tmp_int_train[:, 0, 0:n_fr], axis=1)
                    tmp_int_train_a = np.mean(tmp_int_train[:, 1, 0:n_fr], axis=1)

                    tmp_d = (tmp_int_train_d - self.bg[0]) * self.gamma
                    tmp_a = tmp_int_train_a - self.bg[1]
                    tmp_d_leak = tmp_d * self.leakage
                    tmp_d = tmp_d + tmp_d_leak
                    tmp_a = tmp_a - tmp_d_leak
                    tmp_e = tmp_a / (tmp_d + tmp_a)
                    _, _, fit_coef, _ = self.calc_e_hist(tmp_e, e_bin_edge, npeak=1, debug_mode=False)
                    if not np.isnan(fit_coef[1]):
                        tmp_e_means.append(fit_coef[1])
                        n_success += 1
                rep_e_t_all.append(np.std(tmp_e_means))
            sys.stdout.write(f'\r\n')
            return rep_e_t_all

        if ax == None:
            is_stand_along_fig = True
            if fignum == None:
                fignum = 5121
            fhd = plt.figure(fignum)
            fhd.clf()
            fhd.set_size_inches(3.2, 3)
            ax = fhd.subplots(1, 1)
        else:
            is_stand_along_fig = False
            ax.cla()

        sys.stdout.write(f'\r\n')
        n_fr2integrate = [3, 4, 5, 7, 10, 15, 20, 30, 50]
        tmp_set_mid = []
        n_mol_per_boot1 = 10
        e_t_all_mol1 = get_e_t(mid2ana=self.mid, n_fr2integrate=n_fr2integrate, n_boot=1000, n_mol_per_boot=n_mol_per_boot1)
        n_mol_per_boot2 = 50
        e_t_all_mol2 = get_e_t(mid2ana=self.mid, n_fr2integrate=n_fr2integrate, n_boot=1000, n_mol_per_boot=n_mol_per_boot2)
        n_mol_per_boot3 = 100
        e_t_all_mol3 = get_e_t(mid2ana=self.mid, n_fr2integrate=n_fr2integrate, n_boot=1000, n_mol_per_boot=n_mol_per_boot3)

        # for group_id1, c_group in enumerate(self.seq_group):
        #     if c_group.group_name == 'TATTTATT':
        #         break
        # tmp_set_mid = self.seq_group[group_id1].mid
        # e_t_all_mol2 = get_e_t(mid2ana=tmp_set_mid, n_fr2integrate=n_fr2integrate)
        #
        # for group_id2, c_group in enumerate(self.seq_group):
        #     if c_group.group_name == 'TAAAAATT':
        #         break
        # tmp_set_mid = self.seq_group[group_id2].mid
        # e_t_all_mol3 = get_e_t(mid2ana=tmp_set_mid, n_fr2integrate=n_fr2integrate)

        # fhd = plt.figure(fignum)
        # fhd.set_size_inches(3.2, 3)
        # plt.clf()
        ax.plot(n_fr2integrate, e_t_all_mol1, 'o-', color=(.7, .3, .3), label=f'{n_mol_per_boot1} mols')
        ax.plot(n_fr2integrate, e_t_all_mol2, 'o-', color=(.3, .6, .3), label=f'{n_mol_per_boot2} mols')
        ax.plot(n_fr2integrate, e_t_all_mol3, 'o-', color=(.3, .3, .6), label=f'{n_mol_per_boot3} mols')

        ax.set_xscale('log')
        ax.set_yscale('log')
        ax.set_xlim(1, n_fr2integrate[-1]*2)
        ax.set_ylim(1E-3, 0.1)
        ax.set_xlabel('Observation time (# frames)')
        ax.set_ylabel('FRET Error (sem)')
        ax.grid(True, which='major', linestyle='-', color=(.4, .3, .3))
        ax.grid(True, which='minor', dashes=(5, 2), color=(.8, .8, .9))
        ax.legend()
        # plt.title(f'# mol={n_mol_per_boot}')
        if is_stand_along_fig:
            plt.tight_layout()
            plt.pause(0.1)
            fhd.savefig(os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_nfr_vs_error_{fhd.number}.eps'),
                        format='eps')

    def analyze_base_contents(self, fignum=500, e_lim=0.025):
        def calc_base_count():
            self.nt_counts = []
            for c_group in self.seq_group_sorted_by_e:
                # print('\n' + c_group.group_name)
                nt_count = {'A': 0, 'C': 0, 'G': 0, 'T': 0}
                nt_count['A'] = len([letter for letter in c_group.group_name[1:6] if letter == 'A'])
                nt_count['C'] = len([letter for letter in c_group.group_name[1:6] if letter == 'C'])
                nt_count['G'] = len([letter for letter in c_group.group_name[1:6] if letter == 'G'])
                nt_count['T'] = len([letter for letter in c_group.group_name[1:6] if letter == 'T'])
                self.nt_counts.append(nt_count)
                # print(nt_count)

            # --- Collect FRET values
            e_collected = {'A': [[] for _ in range(6)],
                           'C': [[] for _ in range(6)],
                           'G': [[] for _ in range(6)],
                           'T': [[] for _ in range(6)]}
            n_collected = {'A': [[] for _ in range(6)],
                           'C': [[] for _ in range(6)],
                           'G': [[] for _ in range(6)],
                           'T': [[] for _ in range(6)]}
            for c_group, c_nt_count in zip(self.seq_group_sorted_by_e, self.nt_counts):
                if not np.isnan(c_group.e_mean):
                    if c_group.enough_nmol:
                        for c_base in ['A', 'C', 'G', 'T']:
                            e_collected[c_base][c_nt_count[c_base]].append(c_group.e_mean)
                    for c_base in ['A', 'C', 'G', 'T']:
                        n_collected[c_base][c_nt_count[c_base]].append(len(c_group.seq))

            # replace zero-size array from e_collected with nan array
            for per_base in n_collected:
                for ii, per_count in enumerate(e_collected[per_base]):
                    if len(per_count) == 0:
                        e_collected[per_base][ii] = [np.nan, np.nan,
                                                     np.nan]  # this is a work round of the violinplot error when there is empty dataset
                for ii, per_count in enumerate(n_collected[per_base]):
                    if len(per_count) == 0:
                        n_collected[per_base][ii] = [np.nan, np.nan,
                                                     np.nan]  # this is a work round of the violinplot error when there is empty dataset

            self.e_per_base_count = e_collected
            self.n_per_base_count = n_collected
            self.e_mean_per_base_count, self.e_std_per_base_count = {}, {}
            for bsi, c_base in enumerate(['A', 'C', 'G', 'T']):
                self.e_mean_per_base_count[c_base] = [np.mean(elmt) for elmt in e_collected[c_base]]
                self.e_std_per_base_count[c_base] = [np.std(elmt) for elmt in e_collected[c_base]]

        def calc_tract():
            # check number of tracts in the sequence
            self.tracts = []
            Atract_max = 5
            for c_group in self.seq_group_sorted_by_e:
                tmp_tract = {'A': 0, 'C': 0, 'G': 0, 'T': 0}
                for c_base in ['A', 'C', 'G', 'T']:
                    for a_len in range(Atract_max, -1, -1):
                        if c_base * a_len in c_group.group_name[1:6]:
                            break
                    tmp_tract[c_base] = a_len
                self.tracts.append(tmp_tract)
                # print(c_group.group_name[1:6])
                # print(tmp_tract)

            # collect FRET values per tracts
            e_collected = {'A': [[] for _ in range(6)],
                           'C': [[] for _ in range(6)],
                           'G': [[] for _ in range(6)],
                           'T': [[] for _ in range(6)]}
            e_mean_of_all = []
            for c_group, c_nt_count in zip(self.seq_group_sorted_by_e, self.tracts):
                if not np.isnan(c_group.e_mean) and c_group.enough_nmol:
                    e_mean_of_all.append(c_group.e_mean)
                    for c_base in ['A', 'C', 'G', 'T']:
                        e_collected[c_base][c_nt_count[c_base]].append(c_group.e_mean)
            self.e_mean_among_enough_nmol = np.mean(e_mean_of_all)

            # replace zero-size array from e_collected with nan array
            for per_base in ['A', 'C', 'G', 'T']:
                for ii, per_count in enumerate(e_collected[per_base]):
                    if len(per_count) == 0:
                        e_collected[per_base][ii] = [np.nan, np.nan, np.nan]  # this is a work round of the violinplot error when there is empty dataset

            self.e_per_tract = e_collected
            self.e_mean_per_tract, self.e_std_per_tract = {}, {}
            for bsi, c_base in enumerate(['A', 'C', 'G', 'T']):
                self.e_mean_per_tract[c_base] = [np.mean(elmt) for elmt in e_collected[c_base]]
                self.e_std_per_tract[c_base] = [np.std(elmt) for elmt in e_collected[c_base]]

        def calc_base_count_without_tract():
            # --- calc base counts without consecutive bases
            e_collected = {'A': [[] for _ in range(6)],
                           'C': [[] for _ in range(6)],
                           'G': [[] for _ in range(6)],
                           'T': [[] for _ in range(6)]}
            for c_group, c_tract, c_nt_count in zip(self.seq_group_sorted_by_e, self.tracts, self.nt_counts):
                # print('')
                # print(c_group.group_name)
                # print(' tract: ' + str(c_tract))
                # print(' count: ' + str(c_nt_count))
                if not np.isnan(c_group.e_mean) and c_group.enough_nmol:
                    for base_id, c_base in enumerate(['A', 'C', 'G', 'T']):
                        if c_nt_count[c_base] != c_tract[c_base]:
                            # print(' ' + c_base + ' selected')
                            e_collected[c_base][c_nt_count[c_base]].append(c_group.e_mean)

            # replace zero-size array from e_collected with nan array
            for per_base in ['A', 'C', 'G', 'T']:
                for ii, per_count in enumerate(e_collected[per_base]):
                    if len(per_count) == 0:
                        e_collected[per_base][ii] = [np.nan, np.nan,
                                                     np.nan]  # this is a work round of the violinplot error when there is empty dataset
            self.e_per_base_count_wo_tract = e_collected
            self.e_mean_per_base_count_wo_tract, self.e_std_per_base_count_wo_tract = {}, {}
            for bsi, c_base in enumerate(['A', 'C', 'G', 'T']):
                self.e_mean_per_base_count_wo_tract[c_base] = [np.mean(elmt) for elmt in e_collected[c_base]]
                self.e_std_per_base_count_wo_tract[c_base] = [np.std(elmt) for elmt in e_collected[c_base]]


        calc_base_count()
        calc_tract()
        calc_base_count_without_tract()


        # --- Draw Base count vs.  n mol
        x = np.arange(0, 6)


        fhd = plt.figure(fignum + 1)
        fhd.set_size_inches(6, 3)
        fhd.clf()
        for bsi, c_base in enumerate(['A', 'C', 'G', 'T']):
            plt.subplot(1, 4, bsi + 1)
            plt.violinplot(self.n_per_base_count[c_base], positions=x, showmeans=False, showextrema=False)
            plt.scatter(x, [np.mean(elmt) for elmt in (self.n_per_base_count[c_base])], marker='.',  color=(.1, .1, 1))
            plt.xlabel(c_base + ' counts')
            if bsi == 0:
                plt.ylabel('# molecules')
                plt.yticks(ticks=[0, 200, 400, 600, 800])
            else:
                plt.yticks(ticks=[0, 200, 400, 600, 800], label=[])
            plt.xlim(-0.5, 5.5)
            plt.ylim(0, 700)
            # plt.ylim(1, 1000)
        plt.pause(0.1)
        plt.tight_layout()
        plt.pause(0.5)
        fhd.savefig(
            os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_nmol_vs_base_count_fig{fhd.number}.eps'),
            format='eps')
        fhd.savefig(
            os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_nmol_vs_base_count_fig{fhd.number}.png'),
            format='png')

        # --- Draw Base count vs.  FRET
        x = np.arange(0, 6)
        fhd = plt.figure(fignum + 2)
        fhd.set_size_inches(6, 3)
        fhd.clf()
        for bsi, c_base in enumerate(['A', 'C', 'G', 'T']):
            plt.subplot(1, 4, bsi + 1)
            plt.violinplot(self.e_per_base_count[c_base], positions=x, showmeans=False, showextrema=False)
            plt.scatter(x, [np.mean(elmt) for elmt in (self.e_per_base_count[c_base])], marker='.',  color=(.1, .1, 1))
            plt.plot([-1, 6], [self.e_mean_among_enough_nmol, self.e_mean_among_enough_nmol], '--', color=(.7, .7, .7))
            plt.xlabel(c_base + ' counts')
            if bsi == 0:
                plt.ylabel('FRET')
            plt.xlim(-0.5, 5.5)
            if isinstance(e_lim, float):
                plt.ylim(self.e_mean_among_enough_nmol - e_lim, self.e_mean_among_enough_nmol + e_lim)
            else:
                plt.ylim(e_lim)
        plt.tight_layout()
        plt.pause(0.1)
        fhd.savefig(
            os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_e_vs_base_count_fig{fhd.number}.eps'),
            format='eps')
        fhd.savefig(
            os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_e_vs_base_count_fig{fhd.number}.png'),
            format='png')

        # --- draw tracts


        fhd = plt.figure(fignum +3)
        fhd.set_size_inches(6, 3)
        fhd.clf()
        for bsi, c_base in enumerate(['A', 'C', 'G', 'T']):
            plt.subplot(1, 4, bsi + 1)
            plt.violinplot(self.e_per_tract[c_base], positions=x, showmeans=False, showextrema=False)
            plt.scatter(x, self.e_mean_per_base_count_wo_tract[c_base], marker='.',  color=(.9, .5, .1), label='broken')
            plt.scatter(x, [np.mean(elmt) for elmt in (self.e_per_tract[c_base])], marker='.',  color=(.1, .1, 1), label='stretch')
            plt.plot([-1, 6], [self.e_mean_among_enough_nmol, self.e_mean_among_enough_nmol], '--', color=(.7, .7, .7))
            plt.xlabel(c_base + ' tract (nt)')
            if bsi == 0:
                plt.ylabel('FRET')
                # plt.yticks(ticks=np.arange(0.6, 0.66, 0.01))
            # else:
            # plt.yticks(ticks=np.arange(0.6, 0.66, 0.01), labels=[])
            plt.xticks(ticks=[1, 2, 3, 4, 5])
            plt.xlim(0.5, 5.5)
            if isinstance(e_lim, float):
                plt.ylim(self.e_mean_among_enough_nmol - e_lim, self.e_mean_among_enough_nmol + e_lim)
            else:
                plt.ylim(e_lim)
            if bsi == 3:
                plt.legend(fontsize=8)
        plt.tight_layout()
        plt.pause(0.1)
        fhd.savefig(os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_e_vs_tracts_fig{fhd.number}.eps'), format='eps')
        fhd.savefig(os.path.join(self.output_filepath, 'ana_' + self.group_name + f'_e_vs_tracts_fig{fhd.number}.png'), format='png')

    def analyze_ionic_strength(self, fignum=5000):
        pass


class SequenceGroup_ssDNA(SequenceGroup):
    def __init__(self, group_name=''):
        super().__init__(group_name)
        return

    def show_all_idv_e_hist(self, fignum=221, _seq_group=None, n_row=16, n_col=16):
        if _seq_group is None:
            _seq_group = self.seq_group
        print('Creating figures...')
        n_ax_per_page = n_col * n_row
        n_pages = (len(_seq_group) - 1) // n_ax_per_page + 1
        fhd, ax = [], []
        for pgi in range(n_pages):
            fhd.append(plt.figure(fignum + pgi, figsize=(18, 9.5)))
            fhd[-1].clf()
            ax.append(fhd[-1].subplots(n_row, n_col))

        n_seq_group = len(_seq_group)
        for group_id in range(n_seq_group):
            # for group_id in range(50):
            if group_id % 13 == 0:
                sys.stdout.write(f'\rdrawing histograms...{group_id / n_seq_group * 100:.1f}%')
            pg_id = group_id // n_ax_per_page
            row_id = group_id % n_ax_per_page // n_col
            col_id = group_id % n_ax_per_page % n_col
            # print(f'i: {group_id}, page: {pg_id}, row: {row_id}, col: {col_id}')
            c_ax = ax[pg_id][row_id][col_id]

            self.show_e_hist(e_bin_center=_seq_group[group_id].e_hist['bin_center'],
                             hist_cnt=_seq_group[group_id].e_hist['hist_cnt'],
                             fit_coef=_seq_group[group_id].e_hist['fit_coef'],
                             fit_curv=_seq_group[group_id].e_hist['fit_curv'],
                             ax=c_ax, keyword=c_exp_name)
            if _seq_group[group_id].e_hist['fit_curv'] is not None:
                y_axis_max = np.max(_seq_group[group_id].e_hist['fit_curv'][1]) * 1.5
            else:
                y_axis_max = 1
            c_ax.text(0.1, y_axis_max * 0.7, _seq_group[group_id].group_name[1:-2], fontsize=5)

            c_ax.set_xlim(0, 1)
            c_ax.set_ylim(0, y_axis_max)
            c_ax.set_xticks([])
            c_ax.set_yticks([])

        for pgi in range(n_pages):
            fhd[pgi].tight_layout(pad=0.1, h_pad=0.01, w_pad=0.002)
            renderer = fhd[pgi].canvas.renderer
            fhd[pgi].draw(renderer)
            plt.pause(.1)
            fhd[pgi].savefig(
                os.path.join(self.output_filepath,
                             'ana_' + self.group_name + f'ehist_indvidual_fig{fhd[pgi].number}.eps'),
                format='eps')
            fhd[pgi].savefig(
                os.path.join(self.output_filepath,
                             'ana_' + self.group_name + f'ehist_indvidual_fig{fhd[pgi].number}.png'),
                format='png')
        print('\n  FRET hist drawn!')


class SequenceGroup_ref(SequenceGroup):
    def __init__(self, group_name=''):
        super().__init__(group_name)
        return

    def show_all_idv_e_hist(self, fignum=221, _seq_group=None, n_row=16, n_col=16, keyword=''):
        if _seq_group is None:
            _seq_group = self.seq_group
        print('Calculating FRET for each sequence...')
        n_ax_per_page = n_col * n_row
        n_pages = (len(_seq_group) - 1) // n_ax_per_page + 1
        fhd, ax = [], []
        for pgi in range(n_pages):
            fhd.append(plt.figure(fignum + pgi, figsize=(4, 9.5)))
            fhd[-1].clf()
            ax.append(fhd[-1].subplots(n_row, n_col, squeeze=False))

        n_seq_group = len(_seq_group)
        for group_id in range(n_seq_group):
            sys.stdout.write(f'\rdrawing histograms...{group_id / n_seq_group * 100:.1f}%')
            pg_id = group_id // n_ax_per_page
            row_id = group_id % n_ax_per_page // n_col
            col_id = group_id % n_ax_per_page % n_col
            # print(f'i: {group_id}, page: {pg_id}, row: {row_id}, col: {col_id}')
            c_ax = ax[pg_id][row_id][col_id]

            self.show_e_hist(e_bin_center=_seq_group[group_id].e_hist['bin_center'],
                             hist_cnt=_seq_group[group_id].e_hist['hist_cnt'],
                             fit_coef=_seq_group[group_id].e_hist['fit_coef'],
                             fit_curv=_seq_group[group_id].e_hist['fit_curv'],
                             ax=c_ax, keyword=_seq_group[group_id].group_name)
            if _seq_group[group_id].e_hist['fit_curv'] is not None:
                y_axis_max = np.max(_seq_group[group_id].e_hist['hist_cnt']) * 1.5
            else:
                y_axis_max = 1
            c_ax.text(0.1, y_axis_max * 0.7, _seq_group[group_id].group_name, fontsize=9)

            c_ax.set_xlim(0, 1)
            c_ax.set_ylim(0, y_axis_max)
            c_ax.set_xlabel('FRET')
            c_ax.set_ylabel('# molecules')

        for pgi in range(n_pages):
            # fhd[pgi].tight_layout(pad=0.1, h_pad=0.01, w_pad=0.002)
            fhd[pgi].tight_layout()
            renderer = fhd[pgi].canvas.renderer
            fhd[pgi].draw(renderer)
            plt.pause(.1)
            fhd[pgi].savefig(
                os.path.join(self.output_filepath,
                             'ana_' + self.group_name + f'ehist_indvidual_fig{fhd[pgi].number}.eps'),
                format='eps')
        fhd[pgi].savefig(
            os.path.join(self.output_filepath,
                         'ana_' + self.group_name + f'ehist_indvidual_fig{fhd[pgi].number}.png'),
            format='png')
        print('\n  FRET hist drawn!')


class exp():
    def __init__(self, exp_name='', root_path=None):
        self.exp_name = exp_name
        self.root_path = root_path
        # self.get_raw_data()
        self.get_raw_data_and_group_sequences()

    def get_raw_data_and_group_sequences(self):
        sys.stdout.write('loading data from ' + self.exp_name + '\n')
        with open(os.path.join(self.root_path, 'raw_data_combined_' + self.exp_name + '.pkl'), 'rb') as output_fobj:
            [rawdata_green_ex, rawdata_red_ex] = pickle.load(output_fobj)

        sys.stdout.write('finding molecule with sequence of interest \n')

        # --- define group of sequences
        ssDNA = SequenceGroup_ssDNA(group_name='ssDNA')
        ref = SequenceGroup_ref(group_name='ref')
        ref.seq_unique = ['AAAAAAAA', 'AAAAGGGG', 'TTTTGGGG', 'TTTTTTTT', 'AAAACCCC', 'ATATATAT']
        # junk = SequenceGroup(group_name='junk')
        ssDNA.output_filepath = self.root_path
        ref.output_filepath = self.root_path

        # --- finding molecule with sequence of interest
        _n_mol_total_including_not_sequenced = 0
        for fov_id, [int561_in_fov, seq_in_fov, int642_in_fov] in enumerate(
                zip(rawdata_green_ex['intensity'], rawdata_green_ex['sequence'], rawdata_red_ex['intensity'])):
            sys.stdout.write(f'\r   working on fov {fov_id}')
            _n_mol_total_including_not_sequenced += len(int561_in_fov)
            for mid, [int561_of_mol, seq_of_mol, int642_of_mol] in enumerate(
                    zip(int561_in_fov, seq_in_fov, int642_in_fov)):
                if isinstance(seq_of_mol, str):
                    if seq_of_mol[7:10] == 'ACG' and seq_of_mol[18:21] == 'ATC':
                        is_ref = False
                        if seq_of_mol[10:18] in ref.seq_unique:
                            ref.seq.append(seq_of_mol[10:18])
                            ref.int_greenex.append(int561_of_mol)
                            ref.int_redex.append(int642_of_mol)
                            is_ref = True
                        if seq_of_mol[10] == 'T' and seq_of_mol[16:18] == 'TT':
                            ssDNA.seq.append(seq_of_mol[10:18])
                            ssDNA.int_greenex.append(int561_of_mol)
                            ssDNA.int_redex.append(int642_of_mol)
                        # else:
                            # if not is_ref:
                                # junk.seq.append(seq_of_mol[10:18])

        def get_all_possible_seq_list():
            base_space = ['A', 'C', 'G', 'T']
            _mgroup_seq = []
            for b1 in base_space:
                for b2 in base_space:
                    for b3 in base_space:
                        for b4 in base_space:
                            for b5 in base_space:
                                _mgroup_seq.append('T' + b1 + b2 + b3 + b4 + b5 + 'TT')
            return _mgroup_seq

        ssDNA.seq_unique = get_all_possible_seq_list()
        ssDNA.seq_org = ssDNA.seq.copy()
        ssDNA.mid = np.arange(len(ssDNA.int_greenex))
        ssDNA.mid_org = ssDNA.mid.copy()
        ref.seq_org = ref.seq.copy()
        ref.mid = np.arange(len(ref.int_greenex))
        ref.mid_org = ref.mid.copy()
        sys.stdout.write('\rmolecule sorted out\n')
        print(f'total {_n_mol_total_including_not_sequenced} molecules processed')

        self.ssDNA = ssDNA
        self.ref = ref
        # self.junk = junk
        return
