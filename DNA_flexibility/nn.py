import numpy as np
import pandas as pd
import gc
import torch
from torch import optim, nn, Tensor
from torch.utils.data import TensorDataset, DataLoader
import torch.nn.functional as F

import matplotlib.pyplot as plt
import seaborn as sns

class Net(nn.Module):
    def __init__(self, nb_features):
        super(Net, self).__init__()
        self.ff1 = nn.Linear(nb_features, 5, bias=False)
        self.ff2 = nn.Linear(5, 1, bias=False)
        # self.ff3 = nn.Linear(5, 1, bias=False)
        self.loss_criterion = nn.MSELoss()

    def forward(self, x):
        x = F.relu(self.ff1(x))
        x = F.relu(self.ff2(x))
        # x = F.sigmoid(self.ff3(x))
        return x

    def loss(self, x, target):
        loss = self.loss_criterion(self.forward(x), target)
        return loss

    def train(self, x, target, opt_type='adam'):
        if opt_type == 'adam':
            optimizer = optim.Adam(self.parameters(), lr=0.001,
                                  # weight_decay=1e-3
                                  )
        else:
            optimizer = optim.SGD(self.parameters(), lr=0.001,
                                   # weight_decay=1e-3
                                   )
        self.zero_grad()
        loss = self.loss(x, target)
        loss.backward()
        optimizer.step()
        return loss




def get_nn_weights(x_train, x_test, y_train, y_test, seqs_test, bs_idx, pos_idx_list, pred_df_list, imp_df_list, interaction_df_dict):
    batch_size = 32
    epochs=10

    # y_train = (y_train - np.mean(y_train)) / np.std(y_train)
    # fret_test = (y_test - np.mean(y_test)) / np.std(y_test)

    dl_train = DataLoader(TensorDataset(Tensor(x_train), Tensor(np.expand_dims(y_train, -1))),
                          shuffle=True, num_workers=2, batch_size=batch_size)
    dl_test = DataLoader(TensorDataset(Tensor(x_test), Tensor(np.expand_dims(y_test, -1))),
                         shuffle=True, num_workers=2, batch_size=100)
    x_test_batch, y_test_batch = next(iter(dl_test))
    y_test_batch = y_test_batch.detach().numpy()

    net = Net(x_train.shape[1])
    loss = 0
    for epoch in range(epochs):
        for i, data in enumerate(dl_train, 0):
            loss += net.train(data[0], data[1]).item()
            # if i % 100 == 99:
            #     abs_err_batch = np.mean(np.abs(net.forward(x_test_batch).detach().numpy() - y_test))
            #     # mse_test = np.sqrt(np.mean((net.forward(x_test_batch).detach().numpy() - y_test) ** 2))
            #     print(f'epoch: {epoch+1}, batch: {i+1}, loss: {loss / 100}, AbsErrTest: {np.round(abs_err_batch, 3)}')
            #     loss = 0

    mse_final = net.loss(dl_test.dataset.tensors[0], dl_test.dataset.tensors[1])
    y_pred = net.forward(dl_test.dataset.tensors[0]).detach().numpy()
    abs_err_final = np.mean(np.abs(y_pred - y_test))
    print(f'final absErrTest: {abs_err_final}')

    # Make sorted weights matrix
    ff1_weights = net.ff1.weight.detach().numpy()
    ff2_weights = net.ff2.weight.detach().numpy()
    ff2_order = np.argsort(ff2_weights[0,:])
    ff1_weights_ordered = ff1_weights[ff2_order, :]
    ff2_weights_ordered = ff2_weights[:, ff2_order]

    # store metrics
    pred_df = pd.DataFrame({
        'fret': y_test.reshape(-1), 'fret_pred': y_pred.reshape(-1), 'sequence': seqs_test,
        'model': 'FFNN', 'bs_idx': bs_idx})

    importance_list = np.linalg.norm(ff1_weights, axis=1)
    importance_list /= np.max(importance_list)
    importance_df = pd.DataFrame({'position': pos_idx_list,
                                  'importance': importance_list,
                                  'model': 'FFNN', 'bs_idx': bs_idx})

    pred_df_list.append(pred_df)
    imp_df_list.append(importance_df)
    # gc.collect()

    plt.subplots(figsize=(10,10))
    plt.scatter(y_test.reshape(-1), y_pred.reshape(-1), )


    # # plot weights as heatmap
    # fig, hm_ax = plt.subplots(1, 1, figsize=(12, 12))
    # sns.heatmap(data=ff1_weights_ordered, fmt='s', cmap='coolwarm', ax=hm_ax, vmin=-0.5, vmax=0.5,
    #             # cbar_kws={'ticks': np.arange(0, 1.1, 0.25)}
    #             )
    # hm_ax.set_aspect('equal', adjustable='box')
    # hm_ax.set_yticklabels([str(l) for l in ff2_weights_ordered[0,:].round(3)])
    # hm_ax.set_xticklabels([f'{p}{n}' for p in range(5) for n in 'GACT'])
    # hm_ax.set_ylabel(None)
    # hm_ax.set_xlabel(None)
    # fig.delaxes(fig.axes[1])
    # plt.tight_layout()
    # plt.show()
