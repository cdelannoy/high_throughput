import os, sys, re, pickle, argparse
from pathlib import Path

import numpy as np
import pandas as pd
from sklearn.cluster import KMeans
from math import ceil

import matplotlib as mpl
import matplotlib.pyplot as plt
import seaborn as sns
from data_classes_SHK import exp, SequenceGroup, SequenceGroup_ref, SequenceGroup_ssDNA  # required to load data structures!

sys.path.append(str(Path(__file__).resolve().parents[1]))
from data_simulation.generate_data import SimulatedSequenceGroup, ssDNA, Ref, Seq
from helper_functions import parse_input_path, parse_output_path
from covariance_detection.covariance_detection import covariance_detection

# todo: add 2D clustering with kinetics

parser = argparse.ArgumentParser(description='Detect covariance in pickled read + FRET data files as defined by SHK')
parser.add_argument('--in-dir', type=str, required=True)
parser.add_argument('--out-dir', type=str, required=True)
parser.add_argument('--nb-classes', type=int, default=2)
parser.add_argument('--resolution', type=str, choices=['molecule', 'sequence'], default='molecule')
parser.add_argument('--separation', type=str, choices=['kmeans', 'linear', 'quantile'], default='kmeans')
parser.add_argument('--min-nb-molecules', type=int, default=30)
parser.add_argument('--alpha', type=float, default=0.95)
args = parser.parse_args()

out_dir = parse_output_path(args.out_dir)

exps = {}
in_dir = str(Path(args.in_dir).resolve())
for efn in parse_input_path(args.in_dir, pattern='*.pkl'):
    with open(efn, 'rb') as output_fobj:
        fid = os.path.splitext(Path(efn).name)[0]
        print('reading ' + fid)
        exps[fid] = pickle.load(output_fobj)
        exps[fid].root_path = in_dir
        exps[fid].ssDNA.output_filepath = in_dir
        exps[fid].ref.output_filepath = in_dir
print('data loaded')

edf_dict = {}
p_mat_dict = {}
for exp_id in exps:
    out_subdir = parse_output_path(out_dir + exp_id)
    fret_list = []
    seq_list = []
    exp = exps[exp_id]
    # extract seqs and efficiencies
    for seq_obj in exp.ssDNA.seq_group:
        nb_molecules = len(seq_obj.e_rep)
        if nb_molecules < args.min_nb_molecules: continue
        if args.resolution == 'molecule':
            fret_list.extend(seq_obj.e_rep)
            seq_list.extend([seq_obj.group_name] * len(seq_obj.e_rep))
        elif args.resolution == 'sequence':
            fret_list.append(seq_obj.e_mean)
            seq_list.append(seq_obj.group_name)
        else:
            raise ValueError('resolution type not recognized')
    fret_array = np.array(fret_list)

    # clustering
    if args.separation == 'linear':
        fmin, fmax = np.min(fret_array), np.max(fret_array)
        lims = np.concatenate(([0], np.linspace(fmin, fmax, args.nb_classes - 2), [1]))
        pred_cls = np.zeros(fret_array.size, dtype=int)
        property_dict = {'efret':{}}
        for ci, (lb, rb) in enumerate(zip(lims[:-1], lims[1:])):
            pred_cls[np.logical_and(lb < fret_array, fret_array < rb)] = ci
            property_dict['efret'][ci] = rb
    elif args.separation == 'quantile':
        lims = np.quantile(fret_array, np.linspace(0, 1, args.nb_classes + 1))
        pred_cls = np.zeros(fret_array.size, dtype=int)
        property_dict = {'efret':{}}
        for ci, (lb, rb) in enumerate(zip(lims[:-1], lims[1:])):
            pred_cls[np.logical_and(lb < fret_array, fret_array < rb)] = ci
            property_dict['efret'][ci] = rb
    elif args.separation == 'kmeans':
        fret_array_reshape = fret_array.reshape(-1,1)
        km = KMeans(args.nb_classes).fit(fret_array_reshape)
        km_order_dict = {idx_old:idx_new for idx_old, idx_new in enumerate(km.cluster_centers_.reshape(-1).argsort())}
        property_dict = {'efret': {fi: f for fi, f in enumerate(sorted(km.cluster_centers_.reshape(-1)))}}
        pred_cls = np.vectorize(km_order_dict.__getitem__)(km.predict(fret_array_reshape))
    else:
        raise ValueError('separation method not recognized')

    efret_df = pd.DataFrame({'efret': fret_list, 'km_cls': pred_cls, 'seq': seq_list})
    edf_dict[exp_id] = efret_df

    # save data
    efret_df.to_csv(f'{out_subdir}ssrna_flex_data_{exp_id}.csv')
    cl_dict = {}
    for cl, sdf in efret_df.groupby('km_cls'):
        with open(f'{out_subdir}ssrna_flex_data_{exp_id}_cls{cl}.txt', 'w') as fh:
            fh.write('\n'.join(sdf.seq))
        cl_dict[cl] = list(sdf.seq)
    cl_dict['all'] = seq_list

    # plotting
    fig, ax = plt.subplots(figsize=(7.5,5))
    sns.histplot(x='efret', hue='km_cls', data=efret_df, ax=ax)
    fig.savefig(f'{out_subdir}data_hist_{exp_id}.svg')
    plt.close(fig)

    # covariance detection
    p_mat_dict[exp_id] = {}
    for cl in cl_dict:
        p_mat_dict[exp_id][cl] = {
            'e_fret': property_dict['efret'].get(cl, 'NA'),
            'p_mat': covariance_detection(cl_dict[cl], parse_output_path(f'{out_subdir}cov_detection_{cl}'), args.alpha)}

# make summary plot
for i in range(args.nb_classes):
    # collect matrices
    pm_list = []
    for x in sorted(p_mat_dict):
        if i in p_mat_dict[x]:
            pm_list.append((x, p_mat_dict[x][i]))
    if not len(pm_list):
        continue
    # pm_list = [(x, p_mat_dict[x][i]) for x in sorted(p_mat_dict)]
    nb_plots = len(pm_list)
    n_rows = ceil(nb_plots / 3)
    fig, axes = plt.subplots(n_rows, 3, figsize=(15, 5 * n_rows))
    axes = axes.reshape(-1)
    used_axes = []
    for pmi, pm_tup in enumerate(pm_list):
        exp_id, pm = pm_tup
        shm = sns.heatmap(pm['p_mat'], vmin=0.0, vmax=1.0,
                          cbar=False, annot=True, linewidth=.5, ax=axes[pmi])
        axes[pmi].set_title(f'{exp_id},Efret:{round(pm["e_fret"],2)}')
        used_axes.append(axes[pmi])
    # fig.colorbar(mpl.cm.ScalarMappable(norm=mpl.colors.Normalize(0.5, 1.0)), ax=used_axes)

    plt.tight_layout()
    fig.savefig(f'{out_dir}heatmaps_combined_cl{i}.svg')
    plt.close(fig)