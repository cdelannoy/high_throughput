import argparse, yaml, os, sys, pickle, re
from os.path import dirname
from pathlib import Path
import numpy as np
import pandas as pd

sys.path.append(f'{dirname(Path(__file__).resolve())}/..')
from helper_functions import reverse_complement, num2str_dict

class Seq:
    def __init__(self, seq, efret, nb_efret):
        self.group_name = seq
        self.e_mean = efret
        self.e_rep = [efret] * nb_efret

class ssDNA:
    def __init__(self):
        self.seq_group = []
        self.output_filepath = ''

class Ref:
    def __init__(self):
        output_filepath = ''


class SimulatedSequenceGroup(object):
    def __init__(self):
        self.ssDNA = ssDNA()
        self.ref = Ref()
        self.root_path = ''

    def add_seqs(self, seqs, efret):
        for seq, ef in zip(seqs, efret):
            self.ssDNA.seq_group.append(Seq(seq, ef, 40))


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Generate test data with randomly generated mock holliday junctions, '
                                                 'displaying co-variation.')
    parser.add_argument('--param-yaml', type=str, required=True, nargs='+',
                        help='yaml files containing simulation parameters')
    parser.add_argument('--nb-seqs', type=int, required=True)
    parser.add_argument('--out-txt', type=str, required=True)
    parser.add_argument('--efret' ,type=float)
    args = parser.parse_args()

    param_list = []
    for param_fn in args.param_yaml:
        with open(param_fn, 'r') as fh: param_list.append(yaml.load(fh, yaml.FullLoader))

    meta_list, meta_fret_list = [], []
    for param_dict in param_list:
        # initiate reference sequence randomly
        ref_sequence = np.random.randint(0, 4, size=param_dict['sequence_length'])

        # Enforce complementarity
        if param_dict['complementary_groups'] is not None:
            for cgi in param_dict['complementary_groups']:
                cg = param_dict['complementary_groups'][cgi]
                fws, fwe, rvs, rve = cg['fwd']['start'], cg['fwd']['end'], cg['rev']['start'], cg['rev']['end']
                if fwe - fws != rve - rvs: raise ValueError(f'Error in complementary group {cgi}: fwd and rev parts not of equal length')
                ref_sequence[rvs:rve] = reverse_complement(ref_sequence[fws:fwe])

        seq_array = np.tile(ref_sequence, (args.nb_seqs, 1))
        seq_array = np.random.randint(0, 4, size=(args.nb_seqs, param_dict['sequence_length']))

        # Mutate
        for vpi in param_dict['variable_position_groups']:
            vp = param_dict['variable_position_groups'][vpi]
            rand_shape = (args.nb_seqs, vp['end'] - vp['start'])
            mutation_mod = np.random.randint(1,3, rand_shape)
            mutation_mask = np.random.rand(*rand_shape) > param_dict['std_mutation_prob']
            mutation_mod[mutation_mask] = 0
            seq_array[:, vp['start']:vp['end']] = (seq_array[:, vp['start']:vp['end']] + mutation_mod) % 4


        # Enforce covariant mutation
        for i1 in param_dict['covariant_positions']:
            original_seq = seq_array[:, i1]
            p_covar = param_dict['covariant_positions'][i1]['p_covariance']
            for i2 in param_dict['covariant_positions'][i1]['covariant']:
                random_match_dict = {i: np.random.randint(0,4) for i in range(4)}
                mutation_mask = np.random.rand(args.nb_seqs) < p_covar
                seq_array[mutation_mask, i2] = np.vectorize(random_match_dict.get)(original_seq[mutation_mask])

        # numeric to letters
        seq_array_str = np.vectorize(num2str_dict.get)(seq_array)
        seq_list = [''.join(s) for s in seq_array_str]
        seq_txt = '\n'.join(seq_list)

        # Assign FRET values
        fret_list = np.clip(np.random.normal(*param_dict['fret_default'], len(seq_list)), 0.0, 1.0)
        for fret_pat in param_dict['fret_patterns']:
            fret_pat_dict = param_dict['fret_patterns'][fret_pat]
            fret_idx_list = [idx for idx in range(len(seq_list)) if re.match(fret_pat, seq_list[idx])]
            new_fret = np.clip(np.random.normal(*fret_pat_dict['fret'], len(fret_idx_list)), 0.0, 1.0)
            fret_bool_list = np.random.rand(len(fret_idx_list)) < fret_pat_dict['p_draw']
            fret_bool_list = np.vstack((fret_bool_list, ~fret_bool_list)).T
            new_fret = np.stack((new_fret, fret_list[fret_idx_list])).T[fret_bool_list]
            fret_list[fret_idx_list] = new_fret
        meta_fret_list.append(fret_list)
        meta_list.append(seq_list)

    # save
    out_txt_stem = f'{Path(args.out_txt).resolve().parent}/{Path(args.out_txt).stem}'
    ssg = SimulatedSequenceGroup()

    for ml, efret in zip(meta_list, meta_fret_list):
        pd.DataFrame({'sequence': ml, 'fret': efret}).to_csv(f'{out_txt_stem}.csv')
        # with open(f'{out_txt_stem}', 'w') as fh: fh.write('\n'.join(ml))
        ssg.add_seqs(ml, efret)

    with open(f'{out_txt_stem}.pkl', 'wb') as fh:
        pickle.dump(ssg, fh, protocol=pickle.HIGHEST_PROTOCOL)
